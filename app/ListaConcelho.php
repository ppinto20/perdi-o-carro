<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaConcelho extends Model
{
    public function veiculos() {
		return $this->hasMany(Veiculo::class, 'concelho_id', 'id');
	}

	public function veiculos_number($concelho) {
		return Veiculo::where('concelho_id', $concelho)->count();
	}
}
