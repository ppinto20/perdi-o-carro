<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BemVindo extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    public $total = 30;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'info@mg.perdiocarro.pt';
        $name = 'Perdi o Carro';
        $subject = 'Mensagem de boas vindas - perdiocarro.pt';

        return $this->view('emails.bemvindo')
                    ->text('emails.bemvindo_plain')
                    ->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject);

    }
}
