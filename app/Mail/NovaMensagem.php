<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mensagem;

class NovaMensagem extends Mailable
{
    use Queueable, SerializesModels;

    public $mensagem;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensagem)
    {
        $this->mensagem = $mensagem;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $address = 'info@mg.perdiocarro.pt';
        $name = 'Perdi o Carro';
        $subject = 'Nova Mensagem - perdiocarro.pt';

        return $this->view('emails.novamensagem')
                    ->text('emails.novamensagem_plain')
                    ->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject);
    }
}
