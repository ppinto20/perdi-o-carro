<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MensagemResposta extends Model
{
    protected $fillable = [
    	'mensagem_id', 'user_id', 'mensagem', 'ip'
	];

	public function user() {
		return $this->hasOne(User::class, 'id', 'user_id');
	}
}
