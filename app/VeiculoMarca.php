<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VeiculoMarca extends Model
{	
    public function veiculos()
	{
		return $this->belongsTo(Veiculo::class);
	}
}
