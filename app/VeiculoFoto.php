<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VeiculoFoto extends Model
{
	protected $fillable = [
    	'veiculo_id', 'caminho', 'nome'
	];

    public function veiculo()
	{
	    return $this->belongsTo(Veiculo::class);
	}
}
