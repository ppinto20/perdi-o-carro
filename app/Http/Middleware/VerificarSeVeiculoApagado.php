<?php

namespace App\Http\Middleware;
use App\Veiculo;

use Closure;

class VerificarSeVeiculoApagado
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $veiculo = Veiculo::withTrashed()->where('slug', $request->id)->first();
        
        if ($veiculo->trashed()) {
            return redirect('/');
        }

        return $next($request);
    }
}
