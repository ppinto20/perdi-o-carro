<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        Menu::make('MainNav', function($menu) {

            $menu->add('Início', ['route'  => 'principal',]);

            // $menu->add('Roubados', ['route' => 'roubados',]);

            // $menu->add('Abandonados', ['route' => 'abandonados',]);
            // 
            
            $menu->add('Notícias', ['route' => 'noticias']);

            $menu->add('Ajuda', ['route' => 'faq']);

            $menu->add('Contacte-nos', ['route' => 'contactos']);


        });


        Menu::make('UserNav', function($menu) {

            $menu->add('Os meus anúncios', ['route'  => 'perfil',])->prepend('<i class="fa fa-tachometer" aria-hidden="true"></i>');

            $menu->add('Mensagens', ['route' => 'mensagens',])->active('/mensagem/*')->prepend('<i class="fa fa-comments" aria-hidden="true"></i>');

            // $menu->add('Sugira algo', ['route' => 'sugira',])->prepend('<i class="fa fa-lightbulb-o" aria-hidden="true"></i>');

            //$menu->add('Ajuda', ['route' => 'sugira'])->prepend('<i class="fa fa-question-circle" aria-hidden="true"></i>');

            $menu->add('Definições de conta', ['route' => 'definicoesdeconta'])->prepend('<i class="fa fa-cogs" aria-hidden="true"></i>');

            

            //https://github.com/lavary/laravel-menu#installation 

          //  ->data('color', 'red'); This probably can be used to set a different menu between admins and managers

        });

        Menu::make('MainUserNav', function($menu) {

            $menu->add('Os meus anúncios', ['route'  => 'perfil',])->prepend('<i class="fa fa-tachometer" aria-hidden="true"></i>');

            $menu->add('Mensagens', ['route' => 'mensagens',])->active('/mensagem')->prepend('<i class="fa fa-comments" aria-hidden="true"></i>');

            // $menu->add('Sugira algo', ['route' => 'sugira',])->prepend('<i class="fa fa-lightbulb-o" aria-hidden="true"></i>');

            //$menu->add('Ajuda', ['route' => 'sugira'])->prepend('<i class="fa fa-question-circle" aria-hidden="true"></i>');

            $menu->add('Definições de conta', ['route' => 'definicoesdeconta'])->prepend('<i class="fa fa-cogs" aria-hidden="true"></i>');

            $menu->add('Logout', '#')->link->attr(array('onclick' => 'event.preventDefault();document.getElementById("logout-form").submit();'));


            

            //https://github.com/lavary/laravel-menu#installation 

          //  ->data('color', 'red'); This probably can be used to set a different menu between admins and managers

        });

        return $next($request);
    }
}
