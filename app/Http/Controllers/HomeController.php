<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Veiculo;
use App\VeiculoMarca;
use App\ListaDistrito;
use App\ListaConcelho;
use Mail;
use App\Mail\FormularioContato;

class HomeController extends Controller
{
    public function index() {

    	$veiculos = Veiculo::where('recuperado', 0)->inRandomOrder()->paginate(15);
      	$marcas = VeiculoMarca::orderBy('marca', 'asc')->get();
        $distritos = ListaDistrito::all();
        $concelhos = ListaConcelho::all();

    	return view('welcome', compact('veiculos', 'marcas', 'distritos', 'concelhos'));
    }

    public function search(Request $request) {

        $distritos = ListaDistrito::all();
        $concelhos = ListaConcelho::all();

        $veiculos = Veiculo::query();

        if ($request->matricula) {
            $veiculos = $veiculos->where('matricula', $request->matricula);
        }

        $veiculos = $veiculos->where('recuperado', 0);

        if ($request->marca) {
            $veiculos = $veiculos->where('marca_id', $request->marca);
        }

        $veiculos = $veiculos->paginate(12);

        $marcas = VeiculoMarca::orderBy('marca', 'asc')->get();

        $request->flash();

        return view('procurar', compact('veiculos', 'marcas', 'request', 'distritos', 'concelhos'));
    }

    public function ajuda() {
        return view('faq');
    }

    public function contactos() {
        return view('contactos');
    }

    public function contactos_enviar(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required',
        ]);

        Mail::to('info@perdiocarro.pt')->send(new FormularioContato($request));


        return redirect()->back()->with('status', 'Mensagem enviada com sucesso! Entraremos em contato o mais rapidamente possível.');
    }

    public function roubados() {
        $veiculos = Veiculo::where('categoria', 'Roubado')->orderBy('created_at', 'desc')->paginate(12);
        $marcas = VeiculoMarca::orderBy('marca', 'asc')->get();

        return view('welcome', compact('veiculos', 'marcas'));
    }

    public function abandonados() {
        $veiculos = Veiculo::where('categoria', 'Abandonado')->orderBy('created_at', 'desc')->paginate(12);
        $marcas = VeiculoMarca::orderBy('marca', 'asc')->get();

        return view('welcome', compact('veiculos', 'marcas'));
    }

    public function filtrar($slug) {

        $distrito = ListaDistrito::where('slug', $slug)->first();
        $concelhos = ListaConcelho::where('distrito', $distrito->id)->get();

        $veiculos = Veiculo::query();

        foreach ($concelhos as $concelho) {
            $veiculos->orWhere('concelho_id', $concelho->id);
        }

        $veiculos = $veiculos->inRandomOrder();

        $veiculos = $veiculos->paginate(12);

        $marcas = VeiculoMarca::orderBy('marca', 'asc')->get();

        //$request->flash();
        //
        $distritos = ListaDistrito::all();
        $concelhos = ListaConcelho::all();

        return view('filtrar', compact('veiculos', 'marcas', 'distritos', 'concelhos', 'slug', 'distrito'));
    }
}
