<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Veiculo;
use Auth;

class AdminController extends Controller
{
    public function index() {

    	
    		$veiculos = Veiculo::withTrashed()->paginate(15);
    	
    	
    	$user = Auth::user();

    	return view('admin.dashboard', compact('veiculos', 'user'));
    }

    public function users_index() {
    	$users = User::withTrashed()->paginate(30);

    	return view('admin.users', compact('users'));
    }
}
