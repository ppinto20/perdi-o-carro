<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Veiculo;
use App\VeiculoFoto;
use App\VeiculoMarca;
use App\ListaConcelho;
use App\Mensagem;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Mail;
use App\Mail\BemVindo;
use App\Mail\NovaMensagem;
use Approached\LaravelImageOptimizer\ImageOptimizer;
use Facebook;
use App;
use App\Mail\AnuncioAdicionado;

class VeiculoController extends Controller
{
    public function anuncio() {

    	$marcas = VeiculoMarca::orderBy('marca', 'asc')->get();
    	$concelhos = ListaConcelho::orderBy('concelho', 'asc')->get();

    	return view('novo-anuncio', compact('marcas', 'concelhos'));
    }

    public function guardar(Request $request, ImageOptimizer $imageOptimizer) {

    	$this->validate($request, [
	          'matricula'   => 'required|unique:veiculos',
            'marca' 		  => 'required',
            'categoria' 	=> 'required',
            'localizacao' => 'required',
	        ]);

      if (!Auth::check()) {
        $this->validate($request, [
          'cnome'         => 'required',
          'email'         => 'required|unique:users',
          'cutilizador'   => 'required',
          'cpassword'     => 'required|confirmed|min:6',
          'cpassword_confirmation' => 'required|min:6',
        ]);

        $utilizador = new User([
          'name'        => $request->input('cnome'),
          'email'       => $request->input('email'),
          'categoria'   => 'utilizador',
          'utilizador'  => $request->input('cutilizador'),
          'password'    => Hash::make($request->cpassword),
        ]);

        $utilizador->save();

        Mail::to($utilizador->email)->send(new BemVindo($utilizador));

        Auth::login($utilizador);
      }



      $veiculo = new Veiculo([
        'utilizador' 	=> Auth::user()->id,
        'matricula'  	=> strtoupper($request->input('matricula')),
        'descricao'  	=> $request->input('descricao'),
        'marca_id' 	  => $request->input('marca'),
        'modelo' 	   	=> $request->input('modelo'),
        'categoria'  	=> $request->input('categoria'),
        'concelho_id' => $request->input('localizacao'),
      ]);

      $veiculo->slug = str_slug($veiculo->marca->marca . ' ' . $veiculo->modelo . ' ' . $veiculo->matricula . ' ' . $veiculo->categoria, '-');

      $veiculo->save();

    	$files = $request->file('foto');

      if ($request->hasFile('foto')) {
        
      	foreach ($files as $file) {
          $imageOptimizer->optimizeUploadedImageFile($file);
          
      		 $foto = new VeiculoFoto([
                'veiculo_id'  => $veiculo->id,
                'caminho'     => $file->store('veiculos/'.$veiculo->id, 'public'),
                'nome'        => $file->getClientOriginalName(),
            ]);

            $foto->save();
      	}
      } else {

        // Caso não tenha foto
        $foto = new VeiculoFoto([
            'veiculo_id' => $veiculo->id,
            'caminho' => 'veiculos/carro-sem-foto.png',
            'nome' => 'carro-sem-foto.png',
        ]);

        $foto->save();
      }


      /* SHARE ON SOCIAL MEDIA */

      // $page_access_token = 'EAACEdEose0cBACLDMOplC4zfEJISBXls9XbugKbcjq0ikji9H4lc2xi3XBBfFjIzOT4lDi3ImiKpbJgPibkxZC5uAfiTj2ZAFPWJtsEZA7f548g8tXCkulCYeOMbWHkQ63yBEZAfl0UGmCMvGlVuqtNkI3LC1oW9B9CYZANHUONfhdnI052ZBUW49sSeNLGXQZD';
      // $page_id = '1913203602294957';

      // $data['picture'] = "https://perdiocarro.pt/img/perdiocarrologo.png";
      // $data['link'] = "https://perdiocarro.pt/";
      // $data['message'] = "Partilhem!!";

      // $data['access_token'] = $page_access_token;
      // $post_url = 'https://graph.facebook.com/'.$page_id.'/feed';

      // $ch = curl_init();
      // curl_setopt($ch, CURLOPT_URL, $post_url);
      // curl_setopt($ch, CURLOPT_POST, 1);
      // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      // $return = curl_exec($ch);
      // curl_close($ch);
               
      return redirect('perfil')->with("status", "Anúncio criado com sucesso.");
    }

    public function mostrar($slug) {
    	$anuncio = Veiculo::withTrashed()->where('slug', $slug)->first();

      $anuncio->visualizacoes += 21;

      $anuncio->save();

    	$i = 0;

      list($width, $height) = getimagesize(secure_url('/storage/'.$anuncio->foto->caminho));

      $veiculos = Veiculo::inRandomOrder()->take(4)->get();

      //Mail::to('web-p9dru@mail-tester.com')->send(new AnuncioAdicionado());
      //https://www.mail-tester.com/web-p9dru
      //
      //Mail::to('pedropinto20@live.com.pt', 'Pedro Pinto')->send(new AnuncioAdicionado());

    	return view('anuncio', compact('anuncio', 'i', 'veiculos', 'width', 'height'));
    }


    public function mensagem(Request $request) {

      if ($request->humano == 1 || Auth::check() ) {
        if (!empty($request->utilizador)) {
          // Caso o utilizador esteja logado
          
          $this->validate($request, [
              'informacoes' => 'required',
          ]);

          $veiculo = Veiculo::find($request->input('anuncio'));

          $mensagem = new Mensagem([
              'veiculo_id'    => $request->input('anuncio'),
              'utilizador_id' => $request->input('utilizador'),
              'mensagem'      => $request->input('informacoes'),
              'para'          => $veiculo->utilizador,
            ]);

        } else {
          // Caso o utilizador não esteja logado
          $this->validate($request, [
              'nome'        => 'required',
              'email'       => 'required',
              'informacoes' => 'required',
          ]);

          $veiculo = Veiculo::find($request->input('anuncio'));

          $mensagem = new Mensagem([
              'veiculo_id'    => $request->input('anuncio'),
              'utilizador_id' => $request->input('utilizador'),
              'email'         => $request->input('email'),
              'nome'          => $request->input('nome'),
              'mensagem'      => $request->input('informacoes'),
              'para'          => $veiculo->utilizador,
            ]);

        }

        $mensagem->save();

        Mail::to($veiculo->user->email)->send(new NovaMensagem($mensagem));
      	
      	return redirect()->back()->with("status", "Obrigado. Mensagem enviada!");
      } else {
        return redirect()->back()->withErrors(['Por favor prove que é humano e deslize o slider para o seu lado direito']);
      }
    }
}
