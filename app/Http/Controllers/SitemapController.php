<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Veiculo;
use App\ListaDistrito;

class SitemapController extends Controller
{
    public function index() {

    	// LIST CITIES
    	// LIST BRANDS
    	// LIST CARS
    	// 	- if they have been recovered, a 301 redirect should be in place
    	

    	// Default
    	
    	$statics = array(
    		array(
                'loc' => '', 
                'lastmod' => '', 
                'changefreq' => 'always', 
            ), 
            array(
                'loc' => 'noticias', 
                'lastmod' => '', 
                'changefreq' => 'monthly', 
            ), 
            array(
                'loc' => 'ajuda', 
                'lastmod' => '', 
                'changefreq' => 'never', 
            ), 
            array(
                'loc' => 'roubados', 
                'lastmod' => '', 
                'changefreq' => 'always', 
            ), 
            array(
                'loc' => 'abandonados', 
                'lastmod' => '', 
                'changefreq' => 'always', 
            ), 
            array(
                'loc' => 'procurar', 
                'lastmod' => '', 
                'changefreq' => 'always', 
            ), 
            array(
                'loc' => 'anuncio', 
                'lastmod' => '', 
                'changefreq' => 'always', 
            ), 
            array(
                'loc' => 'novo-anuncio', 
                'lastmod' => '', 
                'changefreq' => 'never', ), 
            array(
                'loc' => 'contactos', 
                'lastmod' => '', 
                'changefreq' => 'never', 
            ),
    	);

    	$distritos = ListaDistrito::orderBy('nome', 'asc')->get();

    	$veiculos = Veiculo::all();



    	$sitemap = '<?xml version="1.0" encoding="UTF-8"?>
			<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


		foreach ($statics as $static) {
			$sitemap .= '
					<url>
					  <loc>' . config('app.url') . $static["loc"] . '</loc>
					  <lastmod>2017-12-08</lastmod>
					  <changefreq>'. $static["changefreq"] .'</changefreq>
					</url>';
		}

        foreach ($veiculos as $veiculo) {
            $sitemap .= '
                    <url>
                      <loc>' . config('app.url') . 'anuncio/' . $veiculo->slug . '</loc>
                      <lastmod>'. $veiculo->updated_at->format('Y-m-d') .'</lastmod>
                      <changefreq>never</changefreq>
                    </url>';
        }

        foreach ($distritos as $distrito) {
            $sitemap .= '
                    <url>
                      <loc>' . config('app.url') . $distrito->slug . '</loc>
                      <lastmod>'. $veiculo->updated_at->format('Y-m-d') .'</lastmod>
                      <changefreq>never</changefreq>
                    </url>';
        }


		$sitemap .= '</urlset>';


		return response()->make($sitemap, '200')->header('Content-Type', 'text/xml');
    	

    }
}
