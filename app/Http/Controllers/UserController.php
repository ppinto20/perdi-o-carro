<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensagem;
use App\Veiculo;
use Auth;
use App\VeiculoFoto;
use App\MensagemResposta;
use App\User;
use Hash;
use App\VeiculoMarca;
use App\ListaConcelho;
use App\Mail\NovaMensagem;
use App\Mail\FormularioContato;
use Mail;
use Approached\LaravelImageOptimizer\ImageOptimizer;

class UserController extends Controller
{
    public function mensagens() {

    	$mensagens = Mensagem::where('para', Auth::user()->id)->orWhere('utilizador_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        
    	return view('user.mensagens', compact('mensagens'));
    }

    public function mensagem($id) {

    	$mensagem = Mensagem::where('id', $id)->first();

        if ($mensagem->utilizador_id == Auth::user()->id || $mensagem->para == Auth::user()->id) {

            $mensagem->lido = 1;

            $mensagem->save();

            $respostas = MensagemResposta::where('mensagem_id', $id)->orderBy('created_at', 'asc')->get();

        	return view('user.mensagem', compact('mensagem', 'respostas'));

        } else {
            abort(503);
        }
    }

    public function index() {
        $anuncios = Veiculo::where('utilizador', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(6);

    	return view('user.profile', compact('anuncios'));
    }

    public function editar_anuncio($slug) {
        $anuncio = Veiculo::where('slug', $slug)->first();

        $marcas = VeiculoMarca::orderBy('marca', 'asc')->get();
        $concelhos = ListaConcelho::orderBy('concelho', 'asc')->get();

        return view('user.editar_anuncio', compact('anuncio', 'marcas', 'concelhos'));
    }

    public function sugestoes() {
    	return view('user.sugerir');
    }

    public function marcar_recuperado($id) {
        $veiculo = Veiculo::where('id', $id)->first();

        if ($veiculo->recuperado == 1) {
            $veiculo->recuperado = 0;
            $status = "Más notícias ... Veículo voltou a ser marcado como não recuperado.";
        } else {
            $veiculo->recuperado = 1;
            $status = "Hooray! Veículo marcado como recuperado.";
        }

        $veiculo->save();

        return redirect()->back()->with("status", $status);
    }

    public function apagar_anuncio($id) {
        $veiculo = Veiculo::find($id);

        $veiculo->delete();

        //$veiculo = VeiculoFoto::where('veiculo_id', $id)->delete();

        return redirect()->back()->with("status", "Veículo eliminado.");
    }

    public function recuperar_anuncio($id) {
        $veiculo = Veiculo::find($id);

        $veiculo->restore();

        //$veiculo = VeiculoFoto::where('veiculo_id', $id)->delete();

        return redirect()->back()->with("status", "Veículo recuperado e ativo.");
    }

    public function enviar_mensagem(Request $request, $id) {

      $this->validate($request, [
        'mensagem'   => 'required',
        ]);

      $mensagem = new MensagemResposta([
        'user_id'     => Auth::user()->id,
        'mensagem'    => $request->input('mensagem'),
        'mensagem_id' => $id,
        'ip'          => 123,
      ]);

      $mensagem->save();

      $mensagem_original = Mensagem::where('id', $id)->first();

      if (Auth::user()->id == $mensagem_original->para) {
        $user = $mensagem_original->user->email; // Email do Rui
      } else {
        $user = $mensagem_original->para->email; // Email do Admin
      }

      Mail::to($user)->send(new NovaMensagem($mensagem));
         
      return redirect()->back()->with("status", "Mensagem enviado com sucesso com sucesso.");
    }

    public function definicoesdeconta() {
        $user = User::find(Auth::user()->id);

        return view('user.definicoes', compact('user'));
    }

    public function editarperfil(Request $request) {

            $user = User::find(Auth::user()->id);

            if ($user->email == $request->email) {
                $this->validate($request, [
                    'email'   => 'required|email',
                ]);
            } else {
                $this->validate($request, [
                    'email'   => 'required|unique:users|email',
                ]);
            }

            $this->validate($request, [
                'nome'   => 'required',
            ]);

            if (!empty($request->password)) {
                $this->validate($request, [
                    'password' => 'required|confirmed|min:6',
                    'password_confirmation' => 'required|min:6',
                ]);
            }

            $user->name = $request->nome;
            $user->email = $request->email;

            if (!empty($request->password)) {
                $user->password = Hash::make($request->password);
            }

            $user->save();

            return redirect()->back()->with("status", "Perfil atualizado com sucesso.");
    }

    public function guardar_editar_anuncio(Request $request, $id, ImageOptimizer $imageOptimizer) {

        $anuncio = Veiculo::find($id);
        
        if ($anuncio->matricula == $request->matricula) {
            $this->validate($request, [
                'matricula'   => 'required',
            ]);
        } else {
            $this->validate($request, [
                'matricula'   => 'required|unique:veiculos',
            ]);
        }

        $this->validate($request, [
            'descricao'     => 'required',
            'marca'         => 'required',
            'categoria'     => 'required',
            'localizacao'   => 'required',
        ]);


        $anuncio->matricula = strtoupper($request->input('matricula'));
        $anuncio->descricao = $request->descricao;
        $anuncio->marca_id = $request->marca;
        $anuncio->modelo = $request->modelo;
        $anuncio->categoria = $request->categoria;
        $anuncio->concelho_id = $request->localizacao;

        $anuncio->slug = str_slug($anuncio->marca->marca . ' ' . $anuncio->modelo . ' ' . $anuncio->matricula . ' ' . $anuncio->categoria, '-');

        $anuncio->save();

        $files = $request->file('foto');

        if ($request->hasFile('foto')) {

            VeiculoFoto::where('veiculo_id', $anuncio->id)->delete();

            foreach ($files as $file) {
              $imageOptimizer->optimizeUploadedImageFile($file);
              
                 $foto = new VeiculoFoto([
                    'veiculo_id'  => $anuncio->id,
                    'caminho'     => $file->store('veiculos/'.$anuncio->id, 'public'),
                    'nome'        => $file->getClientOriginalName(),
                ]);

                $foto->save();
            }
        } else {
            // Caso não tenha foto
            $foto = new VeiculoFoto([
                'veiculo_id' => $veiculo->id,
                'caminho' => 'veiculos/carro-sem-foto.png',
                'nome' => 'carro-sem-foto.png',
            ]);

            $foto->save();
        }
         
        return redirect('editar-anuncio/'.$anuncio->slug)->with("status", "Anúncio guardado com sucesso.");
    }


    public function cancelar_emails() {
        return view('cancelar');
    }

    public function cancelar_emails_guardar(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'mensagem' => 'required',
        ]);

        Mail::to('ruigoncalvescn@gmail.com')->send(new FormularioContato($request));


        return redirect()->back()->with('status', 'Pedido enviado com sucesso. Será removido da nossa lista de emails.');
    }
}
