<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Veiculo;
use App\Mensagem;
use Mail;
use App\Mail\NovaMensagem;
use Auth;
use App\User;

use Response;

class ApiController extends Controller
{

	public function carros() {
		return Veiculo::where('recuperado', 0)->inRandomOrder()->with('marca')->with('foto')->with('concelho')->paginate(20);
	}
    
    public function carro($id) {
		$veiculo = Veiculo::with('marca')->with('concelho')->with('fotos')->findOrFail($id);
		$veiculo->visualizacoes++;
		$veiculo->save();
		return Response::json($veiculo);
	}

	public function mensagem(Request $request) {
		$this->validate($request, [
		  'nome'   		=> 'required',
		  'email'   	=> 'required',
		  'informacoes' => 'required',
		]);

		$veiculo = Veiculo::find($request->anuncio);

		$mensagem = new Mensagem([
		  'veiculo_id'  	=> $request->anuncio,
		  'utilizador_id' 	=> $request->utilizador,
		  'email'   		=> $request->email,
		  'nome'    		=> $request->nome,
		  'mensagem' 		=> $request->informacoes,
		  'para' 			=> $veiculo->utilizador,
		]);

		$mensagem->save();

	    Mail::to($veiculo->user->email)->send(new NovaMensagem($mensagem));
	  	
	  	return response()->json($mensagem);
	}

	public function login(Request $request) {

		$user = User::where('email','=',$request->email)->first();
 		return Auth::loginUsingId($user->id, TRUE);
	}
}
