<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensagem extends Model
{
	protected $table = 'mensagens';

    protected $fillable = [
    	'veiculo_id', 'nome', 'email', 'mensagem', 'para', 'mensagem_id', 'utilizador_id'
	];

	public function veiculo()
	{
	    return $this->hasOne(Veiculo::class, 'id', 'veiculo_id');
	}

	public function user() {
		return $this->hasOne(User::class, 'id', 'utilizador_id');
	}

	public function para() {
		return $this->hasOne(User::class, 'id', 'para');
	}
}
