<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaDistrito extends Model
{
    public function concelho() {
		return $this->hasMany(ListaConcelho::class, 'distrito', 'id');
	}
}
