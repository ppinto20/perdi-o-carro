<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Veiculo extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];

    protected $fillable = [
    	'utilizador', 'matricula', 'descricao', 'marca_id', 'modelo', 'categoria', 'concelho_id'
	];

	public function fotos()
	{
	    return $this->hasMany(VeiculoFoto::class); // Retorna as fotos todas
	}

	public function foto()
	{
	    return $this->hasOne(VeiculoFoto::class); // Retorna apenas uma foto no caso de ter mais do que uma
	}

	public function marca()
	{
		return $this->hasOne(VeiculoMarca::class, 'id', 'marca_id');
	}

	public function concelho()
	{
		return $this->hasOne(ListaConcelho::class, 'id', 'concelho_id');
	}

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'utilizador');
	}
}
