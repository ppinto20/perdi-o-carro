<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::group(['middleware' => ['menu', 'veiculoapagado']], function() {
// 	Route::get('/anuncio/{id}', 'VeiculoController@mostrar');
// });

Route::group(['middleware' => 'menu'], function() {

	// Links Estaticos

	Route::get('/', 'HomeController@index')->name('principal');
	Route::get('/roubados', 'HomeController@roubados')->name('roubados');
	Route::get('/abandonados', 'HomeController@abandonados')->name('abandonados');

	Route::get('/anuncio', 'HomeController@index'); // Mostra a página principal
	Route::get('/anuncio/{id}', 'VeiculoController@mostrar')->middleware('veiculoapagado');
	Route::get('/novo-anuncio', 'VeiculoController@anuncio');
	Route::post('/novo-anuncio', 'VeiculoController@guardar');


	Route::post('/procurar', 'HomeController@search');
	Route::get('/procurar', 'HomeController@search');

	Route::get('/filtrar', 'HomeController@index');
	Route::get('/filtrar/{slug}', 'HomeController@filtrar');


	Route::get('/ajuda', 'HomeController@ajuda')->name('faq');
	Route::get('/contactos', 'HomeController@contactos')->name('contactos');
	Route::post('/contactos', 'HomeController@contactos_enviar');

	Route::get('/noticias', function(){
		return view('noticias');
	})->name('noticias');

	Route::post('/nova-mensagem', 'VeiculoController@mensagem');


	// Authentication Routes...
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('registar', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('registar', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('palavra-passe/redefinir', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('palavra-passe/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('palavra-passe/redefinir/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('palavra-passe/redefinir', 'Auth\ResetPasswordController@reset');

    Route::get('/cancelar-emails', 'UserController@cancelar_emails');
    Route::post('/cancelar-emails', 'UserController@cancelar_emails_guardar');



    // Carros
    //Route::get('/{distrito}/{concelho}/{categoria}/{modelo}/{matricula}', '');

    // Distritos
    //Route::get('/{distrito}', 'HomeController@filtrar');

    // Concelhos
    //Route::get('/{distrito}/{concelho}', '');

    
});


// https://perdicarro.pt/roubado/guimaraes/braga/fiat/punto/20-45-RA
// {Dominio} / {Distrito} / {Concelho} / {Categoria} / {modelo - matricula}

// Carros / Anúncios



// Usuários

Route::group(['middleware' => ['menu', 'auth']], function() {
	
	Route::get('/perfil', 'UserController@index')->name('perfil');
	Route::get('/mensagens', 'UserController@mensagens')->name('mensagens');
	Route::get('/mensagem/{id}', 'UserController@mensagem');
	Route::post('/mensagem/{id}', 'UserController@enviar_mensagem');

	Route::get('/sugerir', 'UserController@sugestoes')->name('sugira');

	Route::post('/perfil/recuperado/{id}', 'UserController@marcar_recuperado');
	Route::delete('/perfil/apagar/{id}', 'UserController@apagar_anuncio');
	Route::post('/perfil/recuperar/{id}', 'UserController@recuperar_anuncio');
	Route::get('/editar-anuncio/{slug}', 'UserController@editar_anuncio');
	Route::post('/editar-anuncio/{id}', 'UserController@guardar_editar_anuncio');

	Route::get('/editar-perfil', 'UserController@definicoesdeconta')->name('definicoesdeconta');
	Route::post('/editar-perfil', 'UserController@editarperfil');

	//Auth::routes();

	Route::get('/admin/dashboard', 'AdminController@index'); 

});





// Route::get('/inner', function () {
//     return view('user.profile');
// });







// Route::get('/anuncio', function () {
//     return view('anuncio');
// });

// OAuth Routes
Route::get('auth/{provider}', 'SocialMediaController@redirectToProvider');
Route::get('auth/{provider}/callback', 'SocialMediaController@handleProviderCallback');


Route::get('sitemap', 'SitemapController@index');