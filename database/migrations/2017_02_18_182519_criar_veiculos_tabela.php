<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarVeiculosTabela extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->integer('utilizador');
            $table->string('matricula');
            $table->text('descricao')->nullable();
            $table->integer('marca_id');
            $table->string('modelo')->nullable();
            $table->integer('categoria');
            $table->boolean('recuperado');
            $table->integer('visualizacoes');
            $table->integer('concelho_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculos');
    }
}
