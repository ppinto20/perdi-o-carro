<section id="footer">
  <footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h2>Sobre Perdi o Carro </h2>

                    <p>Um projecto desenvolvido com o intuito de ajudar e facilitar a recuperação de veículos roubados.</p>
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <h2>Contatos</h2>

                    <p>
                        Email: <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a><br>
                        Ou utilize o formulário <a href="https://perdiocarro.pt/contactos">aqui</a>.
                    </p>
                </div><!-- /.col-* -->

                <div class="col-sm-4">
                    <h2>As nossas redes sociais</h2>

                    <ul class="social-links nav nav-pills">
                        <li><a target="_blank" href="https://twitter.com/perdiocarro"><i class="fa fa-twitter"></i></a></li>
                        <li><a target="_blank" href="https://www.facebook.com/perdiocarro/"><i class="fa fa-facebook"></i></a></li>
                        <!-- <li><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></li> -->
                        <!-- <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li> -->
                    </ul><!-- /.header-nav-social -->
                </div><!-- /.col-* -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-top -->

    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-left">
                © <?php echo date('Y'); ?> Todos os direitos reservados. Desenvolvido por <a target="blank" href="http://rp-solutions.co.uk/">RP Solutions</a>.
            </div><!-- /.footer-bottom-left -->

            <!-- <div class="footer-bottom-right hidden">
                <ul class="nav nav-pills">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="pricing.html">Pricing</a></li>
                    <li><a href="terms-conditions.html">Terms &amp; Conditions</a></li>
                    <li><a href="contact-1.html">Contact</a></li>
                </ul> /.nav
            </div> --><!-- /.footer-bottom-right -->
        </div><!-- /.container -->
    </div>
</footer>
</section>

<form id="logout-form" action="/logout" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>