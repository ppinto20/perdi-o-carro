<!-- Update your html tag to include the itemscope and itemtype attributes. -->

<!-- Optimização de SEO - Meta Tags para social media, etc -->
<title>{{$titlo}} - Perdi o Carro</title>
<meta name="description" content="{{$descricao}}" />

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Partilha de informações sobre carros roubados ou abandonados - Perdi o Carro">
<meta itemprop="description" content="{{$descricao}}">
<meta itemprop="image" content="{{ secure_url('/img/perdiocarrologosquare.jpg') }}">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@perdiocarro">
<meta name="twitter:title" content="Partilha de informações sobre carros roubados ou abandonados - Perdi o Carro">
<meta name="twitter:description" content="{{$descricao}}">
<meta name="twitter:creator" content="@perdiocarro">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="{{ secure_url('/img/perdiocarrologosquare.jpg') }}">

<!-- Open Graph data -->
<meta property="og:title" content="Partilha de informações sobre carros roubados ou abandonados - Perdi o Carro" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ secure_url('') }}" />
<meta property="og:image" content="{{ secure_url('/img/perdiocarrologosquare.jpg') }}" />
<meta property="og:description" content="{{$descricao}}" />
<meta property="og:site_name" content="Perdi o Carro" />
<meta property="fb:app_id" content="1255430974572704" />

<!-- Hreflang tags -->
<!-- <link rel="alternate" href="https://perdiocarro.pt/" hreflang="pt-pt" /> -->