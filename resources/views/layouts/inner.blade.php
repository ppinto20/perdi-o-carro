<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/typed.js"></script>

        <title>Perdi o Carro</title>

        <script type="text/javascript" src="/js/jquery.uploadPreview.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
        <script type="text/javascript" src="/js/jquery.mask.js"></script>

        <script type="text/javascript">
          $(document).ready(function(){
              $('#matricula').mask('AA-AA-AA');

              $("#concelho").select2();
              $("#marca").select2();
          });


      </script>

      <script type="text/javascript">
          $(document).ready(function() {
              for (var i=0; i<5; i++) {
                  $.uploadPreview({
                      input_field: "#image-upload"+i,   // Default: .image-upload
                      preview_box: "#image-preview"+i,  // Default: .image-preview
                      label_field: "#image-label"+i,    // Default: .image-label
                      label_default: "Adicionar",   // Default: Choose File
                      label_selected: "Mudar",  // Default: Change File
                      no_label: false                 // Default: false
                  });
              }
          });
      </script>
    </head>
    <body>
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v2.11&appId=1255430974572704';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

    @include('layouts.header')

        <section id="bannerinner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
                        @yield('banner')
                    </div>
                </div>
            </div>
        </section>

        <section>
          <div class="container">
            <div class="row">
              @yield('content')
            </div>
          </div>
        </section>

        @extends('layouts.footer')

        @extends('layouts.login')
        
    </body>
</html>
