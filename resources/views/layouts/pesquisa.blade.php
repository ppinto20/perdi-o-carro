<div class="container">
    <fieldset>
        <form action="/procurar" method="POST" class="form-inline" role="form">
            <legend class="sr-only">Form title</legend>

            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="input-group">
                  <input type="text" name="matricula" class="form-control" placeholder="Matricula (xx-xx-xx)" id="matricula" value="{{ old('matricula') }}">
                </div>
            </div>

            <div class="col-md-5 col-sm-5 col-xs-12">
                <div class="input-group">
                    <select name="marca" id="marca" class="form-control">
                        <option value="">- Marca -</option>
                        @foreach ($marcas as $marca)
                            <option value="{{$marca->id}}" @if (old('marca') == $marca->id ) selected @endif">{{$marca->marca}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
                <button type="submit" class="btn btn-primary" style="width: 100%;height: 40px;" onclick="ga('send', 'event', 'Button', 'Click', 'Pesquisa');"><i class="fa fa-search" aria-hidden="true"></i> Procurar</button>
            </div>
            {!! csrf_field() !!}
        </form>
    </fieldset>
</div>