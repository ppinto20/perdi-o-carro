<div class="profile-sidebar">
<!-- SIDEBAR USERPIC -->
<div class="profile-userpic hidden-xs">
	<img src="/img/car-icon.png" class="img-responsive" alt="">
</div>
<!-- END SIDEBAR USERPIC -->
<!-- SIDEBAR USER TITLE -->
<div class="profile-usertitle">
	<div class="profile-usertitle-name">
		{{ Auth::user()->name}}
	</div>
	<div class="profile-usertitle-job">
		{{ Auth::user()->categoria}}
	</div>
</div>
<!-- END SIDEBAR USER TITLE -->
<!-- SIDEBAR BUTTONS -->
<div class="profile-userbuttons">
	<!-- <button type="button" class="btn btn-success btn-sm">Follow</button> -->
	<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger btn-sm">Logout</a>
</div>
<!-- END SIDEBAR BUTTONS -->
<!-- SIDEBAR MENU -->
<div class="profile-usermenu">
	{!! $UserNav->asUl( array('class' => 'nav') ) !!}
</div>
<!-- END MENU -->
