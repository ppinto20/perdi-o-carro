<section id="cabecalho">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-2 col-md-4 col-lg-4">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="fa fa-bars"></span>

        </button>
        <h1 id="logo">
            <a href="/"><img src="/img/perdiocarrologo.png" class="img-responsive" style="max-width: 160px;"></a>
        </h1>
        
      </div>

      <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
       <!--  <button type="button" class="btn btn-link">A minha conta</button> -->
        <nav class="navbar collapse navbar-collapse" id="mainNav">
          

          @if (Auth::check()) 
            <div class="hidden-xs">
              <a href="/perfil" class="dropdown-toggle" data-toggle="dropdown"><button style="padding: 0;background: white;color: #00a9e0;border: 0px solid #00a9e0!important;" type="button" class="btn btn-default"><img src="/img/car-icon.png" class="img-responsive" style="max-width: 38px;""></button></a>

              {!! $MainUserNav->asUl( array('class' => 'dropdown-menu') ) !!}


            </div>
          @else
          	<div class="pull-right hidden-xs">
            	<a href="#login-overlay" data-toggle="modal" data-target="#login-overlay"><button style="padding: 7px 14px;background: white;color: #00a9e0;border: 1px solid #00a9e0!important;" type="button" class="btn btn-default"><i class="fa fa-user" aria-hidden="true"></i></button></a>
            </div>
          @endif
          	<div class="pull-right">
          		<a class="hidden-xs" href="/novo-anuncio" onclick="ga('send', 'event', 'Button', 'Click', 'Criar Anúncio');"><button type="button" class="btn btn-default" style="margin-right: 13.9px;"><i class="fa fa-plus" aria-hidden="true"></i> Criar Anúncio</button></a>
          	</div>

          	{!! $MainNav->asUl( array('class' => 'nav navbar-nav navbar-right')) !!}

          <div class="row hidden-sm hidden-md hidden-lg but-mobile">
            <div class="col-xs-6">
              <a href="/novo-anuncio" ><button type="button" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> Criar Anúncio</button></a>
            </div>
            <div class="col-xs-6">
              <a href="/perfil" class="hidden-sm hidden-md hidden-lg"><button type="button" class="btn btn-default" style="background: #f39c12;"><i class="fa fa-user" aria-hidden="true"></i>  O meu perfil</button></a>
            </div>
          </div>
          
        </nav>

        
      </div>

       
    </div>
  </div>
</section>