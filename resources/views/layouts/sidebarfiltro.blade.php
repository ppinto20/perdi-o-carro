<div class="hidden-xs col-sm-3 col-md-3 col-lg-3">
  <div class="fb-page" data-href="https://www.facebook.com/perdiocarro/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/perdiocarro/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/perdiocarro/">Perdi o Carro</a></blockquote></div>
  <h3>Filtrar por Cidade</h3>
  <ul class="list-group list-unstyled">
    @foreach ($distritos as $distrito)
      
    <li class="list-group-item @if (@$slug == $distrito->slug) active @endif">
      
      <a href="/filtrar/{{$distrito->slug}}">{{ $distrito->nome }}</a>
    </li>
    @endforeach
  </ul>

  @if (App::environment('production'))
    <div>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Lateral Cidade -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-9036204650678898"
            data-ad-slot="3530132164"
            data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
  @endif

</div>