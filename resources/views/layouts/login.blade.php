<!-- Pop-up Login -->

<div id="login-overlay" class=" modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="container">
    <div class="row">
      <form id="loginForm" class="form-signin mg-btm" method="POST" action="/login" novalidate="novalidate">
        {{ csrf_field() }}
        <h3 class="heading-desc">
      <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
      Entrar Perdi o Carro</h3>
      <div class="social-box">
        <div class="row mg-btm">
         <div class="col-md-12">
            <a href="{{ url('/auth/facebook') }}" class="btn btn-facebook btn-block">
              <i class="icon-facebook"></i> Entrar com Facebook
            </a>
        </div>
        </div>
        <div class="row mg-btm">
         <div class="col-md-12">
            <a href="{{ url('/auth/google') }}" class="btn btn-google btn-block">
              <i class="icon-facebook"></i> Entrar com G+
            </a>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
              <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter btn-block" >
                <i class="icon-twitter"></i>    Entrar com Twitter
              </a>
          </div>
        </div>
      </div>
      <div class="main">  
         @if ($errors->has('email') || $errors->has('password'))
            <span class="alert alert-danger help-block">
                <strong>Os seus detalhes não coincidem com os nossos dados.</strong>
            </span>
            <script type="text/javascript">
              $(document).ready(function() {
                  $('#login-overlay').modal('show');
              });
          </script>
        @endif
        <input type="text" class="form-control" placeholder="Email" name="email" required>
        <input type="password" class="form-control" placeholder="Password" name="password" required>
       
      <span class="clearfix"></span>  
          </div>
      <div class="login-footer">
      <div class="row">
        <div class="col-xs-12 col-md-7">
          <div class="left-section">
            <a href="https://perdiocarro.pt/palavra-passe/redefinir">Esqueceu-se da password?</a>
            <a href="https://perdiocarro.pt/registar">Registe-se agora</a>
          </div>
        </div>
          <div class="col-xs-12 col-md-5 pull-right">
            <button type="submit" class="btn btn-large btn-primary pull-right">Entrar</button>
          </div>
      </div>
      
      </div>
        </form>
    </div>
  </div>
</div>

<!-- Pop-up Perdi a minha palavra-passe -->

<!-- <div id="perdi-overlay" class=" modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="container">
    <div class="row">
      <form id="loginForm" class="form-signin mg-btm" method="POST" action="/password/email" novalidate="novalidate">
        {{ csrf_field() }}
        <h3 class="heading-desc">
      <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
      Perdi a palavra-passe do <br/>Perdi o Carro</h3>
      <div class="main">  
         @if ($errors->has('email') || $errors->has('password'))
            <span class="alert alert-danger help-block">
                <strong>Os seus detalhes não coincidem com os nossos dados.</strong>
            </span>
            <script type="text/javascript">
              $(document).ready(function() {
                  $('#perdi-overlay').modal('show');
              });
          </script>
        @endif

        <label for="email">Introduza o seu email</label>
        <input id="email" type="email" class="form-control" placeholder="Introduza o seu email" name="email" value="{{ old('email') }}" required>
       
      <span class="clearfix"></span>  
          </div>
      <div class="login-footer">
      <div class="row">
        <div class="col-xs-12 col-md-7">
          <div class="left-section">
            <a data-dismiss="modal" href="#login-overlay" data-toggle="modal" data-target="#login-overlay">Entrar</a><br/>
            <a data-dismiss="modal" href="#registar-overlay" data-toggle="modal" data-target="#registar-overlay">Registe-se agora</a>
          </div>
        </div>
          <div class="col-xs-12 col-md-5 pull-right">
            <button type="submit" class="btn btn-large btn-primary pull-right">Entrar</button>
          </div>
      </div>
      
      </div>
        </form>
    </div>
  </div>
</div> -->

<!-- Pop-up Registar-se -->

<!-- <div id="registar-overlay" class=" modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="container">
    <div class="row">
      <form id="loginForm" class="form-signin mg-btm" method="POST" action="{{ route('register') }}" novalidate="novalidate">
        {{ csrf_field() }}
        <h3 class="heading-desc">
      <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
      Registar Perdi o Carro</h3>
      <div class="social-box">
        <div class="row mg-btm">
         <div class="col-md-12">
            <a href="#" class="btn btn-facebook btn-block">
              <i class="icon-facebook"></i> Entrar com Facebook
            </a>
        </div>
        </div>
        <div class="row mg-btm">
         <div class="col-md-12">
            <a href="#" class="btn btn-google btn-block">
              <i class="icon-facebook"></i> Entrar com G+
            </a>
        </div>
        </div>
        <div class="row">
        <div class="col-md-12">
              <a href="#" class="btn btn-twitter btn-block" >
                <i class="icon-twitter"></i>    Entrar com Twitter
              </a>
          </div>
        </div>
      </div>
      <div class="main">  
         @if ($errors->has('email') || $errors->has('password'))
            <span class="alert alert-danger help-block">
                <strong>Os seus detalhes não coincidem com os nossos dados.</strong>
            </span>
            <script type="text/javascript">
              $(document).ready(function() {
                  $('#registar-overlay').modal('show');
              });
          </script>
        @endif
        <input id="name" type="text" class="form-control" name="name" placeholder="Introduza o seu nome" value="{{ old('name') }}" required autofocus>
        <input id="email" type="email" class="form-control" placeholder="Introduza o seu email" name="email" value="{{ old('email') }}" required>
        <input id="password" type="password" class="form-control" name="password" placeholder="Introduza a sua palavra-passe" required>
        <input id="password-confirm" type="password" class="form-control" placeholder="Confirme a sua palavra-passe" name="password_confirmation" required>
       
      <span class="clearfix"></span>  
          </div>
      <div class="login-footer">
      <div class="row">
        <div class="col-xs-12 col-md-7">
          <div class="left-section">
            <a data-dismiss="modal" href="#login-overlay" data-toggle="modal" data-target="#login-overlay">Entrar</a><br/>
            <a data-dismiss="modal" href="#perdi-overlay" data-toggle="modal" data-target="#perdi-overlay">Esqueceu-se da password?</a>
          </div>
        </div>
          <div class="col-xs-12 col-md-5 pull-right">
            <button type="submit" class="btn btn-large btn-primary pull-right">Registar</button>
          </div>
      </div>
      
      </div>
        </form>
    </div>
  </div>
</div> -->


