<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>

        <title>Perdi o Carro</title>
    </head>
    <body>

    @include('layouts.header')

    <section id="corpo" style="margin-bottom: 35px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="profile-content">
                        <h3><legend>Entrar</legend></h3>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div id="buttons">
                                  <div class="facebook button">
                                    <i class="icon">
                                      <i class="fa fa-facebook">
                                    </i>
                                  </i>
                                  <div class="slide">
                                    <a href="{{ url('/auth/facebook') }}"><p>facebook</p></a>
                                  </div>
                                  </div>
                                  
                                  <div class="twitter button">
                                    <i class="icon">
                                      <i class="fa fa-twitter">
                                    </i>
                                  </i>
                                  <div class="slide">
                                    <a href="{{ url('/auth/twitter') }}"><p>twitter</p></a>
                                  </div>
                                  </div>
                                  
                                  <div class="google button">
                                    <i class="icon">
                                      <i class="fa fa-google-plus">
                                    </i>
                                  </i>
                                  <div class="slide">
                                    <a href="{{ url('/auth/google') }}"><p>google+</p></a>
                                  </div>
                                  </div>
                                  
                                  
                                  <div class="linkedin button">
                                    <i class="icon">
                                      <i class="fa fa-linkedin">
                                    </i>
                                  </i>
                                  <div class="slide">
                                    <a href=""><p>linkedin</p></a>
                                  </div>
                                    
                                  </div>
                                </div>
                            </div>
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-mail</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" placeholder="alguem@email.com" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Palavra-Passe</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" placeholder="A sua palavra-passe" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lembrar
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Entrar
                                        </button>
                                    </div>

                                    <div class="col-md-12">
                                      <a class="btn btn-link" href="{{ route('password.request') }}">
                                          Esqueceu-se da palavra-passe?
                                      </a>
                                    </div>

                                    <div class="col-md-12">
                                      <a class="btn btn-link" href="{{ route('register') }}">
                                          Registar-se
                                      </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

        @extends('layouts.footer')

    @extends('layouts.login')

    </body>
</html>

