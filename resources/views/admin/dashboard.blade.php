<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>

        <title>Perdi o Carro</title>
    </head>
    <body>

    @include('layouts.header')

    <section id="corpo" style="margin-bottom: 35px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<h3>Lista de anúncios</h3>
                	<div class="profile-content">
	                    <table class="table table-striped table-hover">
	                      <thead>
	                        <tr>
	                          <th><a href="?orderBy=user">User</a></th>
	                          <th>Matrícula</th>
	                          <th>Marca</th>
	                          <th>Modelo</th>
	                          <th>Categoria</th>
	                          <th>Recuperado</th>
	                          <th>Visualizações</th>
	                        </tr>
	                      </thead>
	                      <tbody>
	                        @foreach ($veiculos as $veiculo)
	                          <tr>
	                            <td>{{$veiculo->user->name}}</td>
	                            <td>{{$veiculo->matricula}}</td>
	                            <td>{{$veiculo->marca->marca}}</td>
	                            <td>{{$veiculo->modelo}}</td>
	                            <td>{{$veiculo->categoria}}</td>
	                            <td>@if ($veiculo->recuperado == 0)
	            					<i class="fa fa-times-circle" aria-hidden="true" style="color: red;"></i>
	                                @else
	                                <i class="fa fa-check-circle" aria-hidden="true" style="color: green;"></i>
	                                @endif</td>
	                            <td>{{$veiculo->visualizacoes}}</td>
	                          </tr>
	                        @endforeach
	                      </tbody>
	                    </table>
	                    {{ $veiculos->links() }}
	                </div>
                </div>
            </div>
        </div>
    </section>

        @extends('layouts.footer')

    @extends('layouts.login')

    </body>
</html>

