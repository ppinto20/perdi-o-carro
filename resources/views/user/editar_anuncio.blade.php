@extends('layouts.inner')

@section('banner')

	<h1 class="text-center">A minha conta</h1>

@endsection

@section('content')

	<div class="row profile">
			<div class="col-md-3">
				@include('layouts.sidebar')
			</div>
		</div>
		<div class="col-md-9">
        <div class="profile-content">
          <h3>Editar Anúncio </h3>
          @include('layouts.flash')
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <form action="/editar-anuncio/{{$anuncio->id}}" method="POST" class="dropzone1" role="form" style="margin-top: 25px;" enctype="multipart/form-data">
            {{ csrf_field() }}

            <fieldset class="form-group">
                <label for="inputEmail3">Matrícula</label>
                <div>
                  <input type="text" name="matricula" class="form-control" id="matricula" value="{{ $anuncio->matricula }}" placeholder="20-GA-30" required>
                </div>
            </fieldset>

            <fieldset class="form-group">
               <label for="inputEmail3">Descrição</label>
               <div >
                 <textarea name="descricao" class="form-control" rows="6" placeholder="Descrição">{{ $anuncio->descricao }}</textarea>
               </div>
            </fieldset>

          <fieldset class="form-group">
              <div class="titulo_separador"><span>Dados do veículo</span></div>

              <div class="row">

                <div class="col-md-6">
                    <label>Marca</label>
                    <div>
                        <select required="" name="marca" id="marca" class="form-control">
                            @foreach ($marcas as $marca)
                                <option value="{{$marca->id}}" @if ($anuncio->marca_id == $marca->id) selected @endif>{{$marca->marca}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <label for="inputEmail3">Modelo</label>
                    <div >
                      <input type="text" name="modelo" class="form-control" id="inputEmail3" placeholder="Focus 1.6 TDI" value="{{ $anuncio->modelo }}">
                    </div>
                </div>

            </div>

         </fieldset>

         <fieldset class="form-group">

            <div class="titulo_separador"><span>Última Localização</span></div>

            <label>Concelho</label>
            <select required="" name="localizacao" id="concelho" class="form-control">
                @foreach ($concelhos as $concelho)
                    <option value="{{$concelho->id}}" @if ($anuncio->concelho_id == $concelho->id) selected @endif>{{$concelho->concelho}}</option>
                @endforeach  
            </select>

         </fieldset>

         <fieldset class="form-group">
            <div class="titulo_separador"><span>Fotos</span></div>

            <label>Mudar fotos</label>
            <div class="">
              @php $i = 0 @endphp
              @foreach ($anuncio->fotos as $foto)
                <div id="image-preview{{$i}}" class="ip" style="background-image: url('{{ Storage::url('' . $foto->caminho) }}');background-size: cover;background-position: center center;">
                  <label for="image-upload{{$i}}" id="image-label{{$i}}" class="il">Mudar</label>
                  <input type="file" name="foto[]" id="image-upload{{$i}}" class="iu" />
                </div>
                @php $i++ @endphp
              @endforeach

              @for ($i; $i < 5; $i++)
                <div id="image-preview{{$i}}" class="ip">
                    <label for="image-upload{{$i}}" id="image-label{{$i}}" class="il">Adicionar</label>
                    <input type="file" name="foto[]" id="image-upload{{$i}}" class="iu" />
                </div>
              @endfor

            </div>

            <small><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Não deixe o anúncio sem imagem, caso não tenha nenhuma do seu veículo por favor utilize uma da internet</small>
         </fieldset>

         <fieldset class="form-group">
            <div class="titulo_separador"><span>Categoria</span></div>

            <div class="radio">
              <label class="radio-inline">
                <input type="radio" name="categoria" value="Roubado" @if ($anuncio->categoria == 'Roubado') checked @endif> Roubado
              </label>

              <label class="radio-inline">
                <input type="radio" name="categoria" value="Abandonado" @if ($anuncio->categoria == 'Abandonado') checked @endif> Abandonado
              </label>
            </div>

        </fieldset>

             

          <fieldset class="form-group">
              <button type="submit" class="btn btn-primary pull-right">Guardar alterações</button>
          </fieldset>
        </form>
        </div>
		</div>
	</div>

@endsection