@extends('layouts.inner')

@section('banner')

	<h1 class="text-center">A minha conta</h1>

@endsection

@section('content')

	<div class="row profile">
			<div class="col-md-3 col-xs-12">
				@include('layouts.sidebar')
			</div>
		</div>
		<div class="col-md-9 col-xs-12">
      <div class="profile-content">
       <h3>Os meus anúncios</h3>
       @include('layouts.flash')
       <div class="list-group">
          @if ($anuncios->isEmpty())
            <p>Ooooh! Ainda não criou nenhum anúncio. <a href="/novo-anuncio">Clique aqui</a> para criar um, não há tempo a perder!</p>
          @else
            @foreach ($anuncios as $anuncio)
                <div class="list-group-item clearfix @if ($anuncio->trashed()) apagado @endif">
                    <div class="profile-teaser-left">
                        <div class="profile-img"><a href="/anuncio/{{$anuncio->slug}}"><img src="/storage/{{$anuncio->foto->caminho}}"/></a></div>
                    </div>
                    <div class="profile-teaser-main">
                        <a href="/anuncio/{{$anuncio->slug}}"><h3 class="profile-name">{{$anuncio->matricula}}</h3></a>
                        <div class="profile-info">
                            <div class="info"><span class="">Visualizações:</span> {{$anuncio->visualizacoes}}</div>
                            <div class="info"><span class="">Início:</span> {{$anuncio->created_at->format('d-m-Y')}}</div>
                            <div class="info"><span class="">Estado:</span> {{$anuncio->categoria}} @if ($anuncio->recuperado != 0) <span class="label label-success">Recuperado</span> @endif</div>
                        </div>
                    </div>
                    <div class="profile-teaser-right">
                      @if ($anuncio->trashed()) 
                         <form action="/perfil/recuperar/{{$anuncio->id}}" method="POST" role="form">
                          {{ csrf_field() }}
                            <div class="info"><span class=""><button type="submit" class="btn btn-default btn-sm"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> Publicar</button></span></div>
                        </form>
                      @else
                        <form action="/perfil/recuperado/{{$anuncio->id}}" method="POST" role="form">
                          {{ csrf_field() }}
                          <div class="info"><span class=""><button type="submit" class="btn btn-default btn-sm">@if ($anuncio->recuperado == 0)<i class="fa fa-check" aria-hidden="true"></i> Recuperado @else <i class="fa fa-times" aria-hidden="true"></i> Não Recuperado @endif</button></span></div>
                        </form>

                        <div class="info"><span class=""><a href="/editar-anuncio/{{$anuncio->slug}}"><button type="button" class="btn btn-default btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</button></a></span></div>

                        <form action="/perfil/apagar/{{$anuncio->id}}" method="POST" role="form">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                            <div class="info"><span class=""><button type="submit" class="btn btn-default btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Apagar</button></span></div>
                        </form>
                      @endif
                    </div>
                </div><!-- item -->
            @endforeach
          @endif

          {{ $anuncios->links() }}
          </div>
      </div>
		</div>
	</div>



@endsection