@extends('layouts.inner')

@section('banner')

  <h1 class="text-center">A minha conta</h1>

@endsection

@section('content')

  <div class="row profile">
      <div class="col-md-3">
        @include('layouts.sidebar')
      </div>
    </div>
    <div class="col-md-9">
      <div class="profile-content">
        <h3>Mensagens de: @if (empty($mensagem->user->name)) {{$mensagem->nome}}  @else {{$mensagem->user->utilizador}} @endif</h3>
        @include('layouts.flash')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ($mensagem->utilizador_id == Auth::user()->id)

          <div class="row msg_container base_sent">
              <div class="col-md-11 col-xs-10">
                  <div class="mensagens msg_sent">
                      <p>{{$mensagem->mensagem}}</p>
                      <time datetime="{{$mensagem->created_at}}">Tu • {{$mensagem->created_at->diffForHumans()}}</time>
                  </div>
              </div>
              <div class="col-md-1 col-xs-2 avatar">
                  <img src="/img/car-icon.png" class="img-responsive" alt="">
              </div>
          </div>

        @else 

          <div class="row msg_container base_received">
              <div class="col-md-1 col-xs-1 avatar">
                  <img src="/img/car-icon.png" class="img-responsive" alt="">
              </div>
              <div class="col-md-11 col-xs-10">
                  <div class="mensagens msg_received">
                      <p>{{$mensagem->mensagem}}</p>
                      <time datetime="{{$mensagem->created_at}}">@if (empty($mensagem->user->name)) {{$mensagem->nome}}  @else {{$mensagem->user->utilizador}} @endif • {{$mensagem->created_at->diffForHumans()}}</time>
                  </div>
              </div>
          </div>

        @endif

        @if (!empty($mensagem->user->name))

          @foreach ($respostas as $resposta)
              @if ($resposta->user_id == $mensagem->utilizador_id)
                
                <div class="row msg_container base_received">
                      <div class="col-md-1 col-xs-1 avatar">
                          <img src="/img/car-icon.png" class="img-responsive" alt="">
                      </div>
                      <div class="col-md-11 col-xs-10">
                          <div class="mensagens msg_received">
                              <p>{{$resposta->mensagem}}</p>
                              <time datetime="{{$resposta->created_at}}">{{$resposta->user->utilizador}} • {{$resposta->created_at->diffForHumans()}}</time>
                          </div>
                      </div>
                  </div>

              @else

                <div class="row msg_container base_sent">
                      <div class="col-md-11 col-xs-10">
                          <div class="mensagens msg_sent">
                              <p>{{$resposta->mensagem}}</p>
                              <time datetime="{{$resposta->created_at}}">Tu • {{$resposta->created_at->diffForHumans()}}</time>
                          </div>
                      </div>
                      <div class="col-md-1 col-xs-2 avatar">
                          <img src="/img/car-icon.png" class="img-responsive" alt="">
                      </div>
                  </div>
                
              @endif
          @endforeach



        <div class="panel-footer">
            <form action="/mensagem/{{$mensagem->id}}" method="POST" role="form">
            {{ csrf_field() }}
                <textarea id="btn-input" rows="2" name="mensagem" type="text" class="form-control" placeholder="Escreva a sua mensagem" /></textarea>

                <button type="submit" class="btn btn-primary" style="margin-top: 15px;" id="btn-chat">Enviar</button>
            </form>
        </div>

        @else 

        <div class="alert alert-info" style="margin-top: 50px;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          
          Esta mensagem foi enviada por um utilizador que não se encontra registado no nosso website, como tal, não poderá utilizar o nosso sistema de mensagem. Caso deseje entrar em contato com este utilizador deverá fazê-lo por email. 
          <br><br>
          Dados do utlizador:
          <br/>
          <b> {{$mensagem->nome}} - <a href="mailto:{{$mensagem->email}}">{{$mensagem->email}}</a></b>.
          <hr>
          Aconselhamos a ter cuidado, especialmente, com as informações partilhadas! Em caso de dúvida ou incerteza, entre em contato connosco <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a>.
        </div>

        @endif
        
      </div>
    </div>
  </div>


<style type="text/css">
.msg_container_base{
  background: #e5e5e5;
  margin: 0;
  padding: 0 10px 10px;
  max-height:300px;
  overflow-x:hidden;
}
.top-bar {
  background: #666;
  color: white;
  padding: 10px;
  position: relative;
  overflow: hidden;
}
.msg_receive{
    padding-left:0;
    margin-left:0;
}
.msg_sent{
    padding-bottom:20px !important;
    margin-right:0;
}
.mensagens {
  background: white;
  padding: 10px;
  border-radius: 2px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  max-width:100%;
}
.mensagens > p {
    font-size: 13px;
    margin: 0 0 0.2rem 0;
  }
.mensagens > time {
    font-size: 11px;
    color: #ccc;
}
.msg_container {
    padding: 10px;
    overflow: hidden;
    display: flex;
}
img {
    display: block;
    width: 100%;
}
.avatar {
    position: relative;
}
.base_receive > .avatar:after {
    content: "";
    position: absolute;
    top: 0;
    right: 0;
    width: 0;
    height: 0;
    border: 5px solid #FFF;
    border-left-color: rgba(0, 0, 0, 0);
    border-bottom-color: rgba(0, 0, 0, 0);
}

.base_sent {
  justify-content: flex-end;
  align-items: flex-end;
}
.base_sent > .avatar:after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    width: 0;
    height: 0;
    border: 5px solid white;
    border-right-color: transparent;
    border-top-color: transparent;
    box-shadow: 1px 1px 2px rgba(black, 0.2); // not quite perfect but close
}

.msg_sent > time{
    float: right;
}



.msg_container_base::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar
{
    width: 12px;
    background-color: #F5F5F5;
}

.msg_container_base::-webkit-scrollbar-thumb
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #555;
}

.btn-group.dropup{
    position:fixed;
    left:0px;
    bottom:0;
}

.mensagens.msg_sent {
    background: #3498db;
}

.mensagens.msg_sent p {
    color: #fff;
}
  </style>

@endsection
