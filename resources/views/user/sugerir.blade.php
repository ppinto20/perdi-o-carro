@extends('layouts.inner')

@section('banner')

	<h1 class="text-center">A minha conta</h1>

@endsection

@section('content')

	<div class="row profile">
			<div class="col-md-3">
				@include('layouts.sidebar')
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">

			   <form action="" method="POST" role="form">
			   	<legend>Fale connosco</legend>
			   	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			   	tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			   	quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			   	consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			   	cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			   	proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

			   	<hr>

			   	<div class="form-group">
			   		<label for="">Nome</label>
			   		<input type="text" class="form-control" id="" placeholder="Input field">
			   	</div>

			   	<div class="form-group">
			   		<label for="">Email</label>
			   		<input type="text" class="form-control" id="" placeholder="Input field">
			   	</div>

			   	<div class="form-group">
			   		<label for="">Sugestão</label>
			   		<textarea rows="5" class="form-control" id="" placeholder="Input field"></textarea>
			   	</div>
			   
			   	
			   
			   	<button type="submit" class="btn btn-primary">Submit</button>
			   </form>
            </div>
		</div>
	</div>

	<style type="text/css">
	a.list-group-item {
	    height:auto;
	    min-height:220px;
	}
	a.list-group-item.active small {
	    color:#fff;
	}

		/***
User Profile Sidebar by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
***/

/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
	</style>

@endsection