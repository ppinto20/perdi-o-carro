@extends('layouts.inner')

@section('banner')

	<h1 class="text-center">A minha conta</h1>

@endsection

@section('content')

	<div class="row profile">
			<div class="col-md-3">
				@include('layouts.sidebar')
			</div>
		</div>
		<div class="col-md-9">
      <div class="profile-content">
       <h3>O meu perfil</h3>
        @include('layouts.flash')

         @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          
          <form action="/editar-perfil" class="form-horizontal" method="POST" role="form">
            {{ csrf_field() }}
            <div class="col-md-6 col-lg-offset-3">
              <div class="form-group">
                <label for="nome">Nome</label>  
                <input id="textinput" name="nome" type="text" value="{{ $user->name }}" class="form-control input-md">
              </div>

              <div class="form-group">
                <label for="email">Email</label>  
                <input id="textinput" name="email" type="email" value="{{ $user->email }}" class="form-control input-md">
              </div>

              <div class="form-group">
                <label for="password">Password</label>  
                <input id="textinput" name="password" type="password" placeholder="Deixar em branco se não quiser alterar" class="form-control input-md">
              </div>

              <div class="form-group">
                <label for="password_confirmation">Confirmação de Password</label>  
                <input id="textinput" name="password_confirmation" type="password" placeholder="Deixar em branco se não quiser alterar" class="form-control input-md">
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">Gravar</button>
              </div>
          </div>
        </form>

      </div>
		</div>
	</div>



@endsection