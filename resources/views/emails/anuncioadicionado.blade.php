<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Carro roubado? - Perdi o Carro</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"/>
</head>

  <body style="margin: 0; padding: 10px 0px;font-family: 'Open Sans', sans-serif;" bgcolor="#E1E8ED">
 <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;border: 0px solid #cccccc;">
 <tr>
  <td align="center" bgcolor="#ffffff" style="padding: 40px 0 30px 0;">
   <img src="https://perdiocarro.pt/img/perdiocarrologo.png" alt="Perdi o Carro logo" width="160" height="23" style="display: block;" />
  </td>
 </tr>
</table>
  
 <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
 <tr>
   <td bgcolor="#fcfcfc" style="padding: 40px 80px 40px 80px;">
     <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td style="color: #153643; text-align: center;font-family: 'Open Sans', sans-serif; font-size: 24px;padding: 0 0 50px;">
         <b>Olá Pedro,</b>
      </td>
      </tr>
      <tr>
       <td style="color: #153643; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 20px;padding: 0 0 15px;text-align: justify;">
        Sabemos que nunca é fácil perder algo tão importante como um carro e por isso criamos um novo website a pensar em si e possivelmente ajudar a encontrar o mesmo.
       </td>
      </tr>
      <tr>
       <td style="color: #153643; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 20px;padding: 0 0 15px;text-align: justify;">
        O nosso website é recente e tomamos a liberdade de adicionar à nossa base de dados os dados de vários carros roubados, incluíndo o seu carro.
       </td>
      </tr>
       <tr>
       <td style="color: #153643; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 20px;padding: 0 0 30px;text-align: justify;">
        O nosso serviço é totalmente gratuito, mas para poder receber actualizações sobre o seu carro gostávamos que se juntasse a nós e se inscreva no perdiocarro.pt.
       </td>
      </tr>
       <tr>
       <td style="color: #153643; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 20px;padding: 0 0 15px;text-align: justify;">
       <ul>
          <li>Os seus dados pessoais nunca serão partilhados com terceiros;</li>
          <li>Os seus dados nunca serão partilhados com outros usuários;</li>
          <li>O seu carro será partilhado nas nossas redes sociais automaticamente;</li>
          <li>Deverá fazer uso do nosso sistema de mensagens para garantir a sua privacidade;</li>
          <li>Não enviamos SPAM;</li>
          <li>Este serviço é totalmente gratuito!</li>
        </ul>
       </td>
      </tr>
       <tr>
       <td style="color: #153643; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 20px;padding: 0 0 15px;text-align: justify;">
        Se não está interessado em que os dados do seu carro sejam divulgados e todos os detalhes sejam removidos no website por favor contacte <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a>.
       </td>
      </tr>
     </table>
    </td>
 </tr>

 <tr>
  <td bgcolor="#f3f3f3" style="padding: 30px 30px 30px 30px;">
   <table border="0" cellpadding="0" cellspacing="0" width="100%">
     <tr>
       <td style="color: #b3b3b3; font-family: 'Open Sans', sans-serif; font-size: 14px;text-align:center;" width="100%">
       © 2017 Todos os direitos reservados. perdiocarro.pt<br/>
       Para deixar de receber os nossos emails <a href="https://perdiocarro.pt/cancelar-emails" style="color: #b3b3b3;"><font color="#b3b3b3">clique aqui</font>.</a>
      </td>

     </tr>
    </table>
  </td>
 </tr>
</table>
  
  
</body>
</html>
