Bem-vindo(a) {{ $user->name }},
Sabemos que nunca é fácil perder algo tão importante como um carro e por isso faremos de tudo para o ajudar a recuperar!

* Os seus dados pessoais nunca serão partilhados com terceiros;
* Os seus dados nunca serão partilhados com outros usuários;
* O seu carro será partilhado nas nossas redes sociais automaticamente;
* Deverá fazer uso do nosso sistema de mensagens para garantir a sua privacidade;
* Não enviamos SPAM;
* Este serviço é totalmente gratuito!

© 2017 Todos os direitos reservados. perdiocarro.pt