Recebeu uma mensagem através do formulário de contacto
* Nome: {{$request->nome}}
* Email: {{$request->email}}
* Mensagem: {{$request->mensagem}}

Caso deseje cancelar a sua conta ou não receber mais emails deste tipo por favor entre em contacto connosco em: info@perdiocarro.pt
© 2017 Todos os direitos reservados. perdiocarro.pt