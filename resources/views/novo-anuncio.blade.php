<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>

        @include('layouts.head.tracking')

        @include('layouts.head.metas', [
            'titlo' => 'Insira um novo anúncio e partilhe com a nossa comunidade', 
            'descricao' => 'Partilha de informações sobre carros roubados ou abandonados. Partilhe informações e ajude a comunidade, hoje é para alguém mas amanhã podes ser tu a precisar.']
        )

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/dropzone.css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
        <script type="text/javascript" src="/js/jquery.uploadPreview.min.js"></script>
        <script type="text/javascript" src="/js/jquery.mask.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.matricula').mask('AA-AA-AA');
                $("#concelho").select2();
                $("#marca").select2();

                for (var i=0; i<10; i++) {
                    $.uploadPreview({
                        input_field: "#image-upload"+i,   // Default: .image-upload
                        preview_box: "#image-preview"+i,  // Default: .image-preview
                        label_field: "#image-label"+i,    // Default: .image-label
                        label_default: "Adicionar",   // Default: Choose File
                        label_selected: "Mudar",  // Default: Change File
                        no_label: false                 // Default: false
                    });
                }
            });
            </script>

    </head>
    <body>

        @include('layouts.header')


        <section id="corpo">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-9 col-md-9 col-lg-8 col-lg-offset-2 col-sm-offset-0">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="/novo-anuncio" method="POST" class="dropzone1" role="form" enctype="multipart/form-data">

            <div class="row tab_container">
            <input id="tab1" class="tabhinput" type="radio" name="categoria" value="Roubado" checked>
            <label class="tabh" for="tab1"><span>Roubado</span></label>

            <input id="tab2" class="tabhinput" type="radio" name="categoria" value="Abandonado">
            <label class="tabh" for="tab2"><span>Abandonado</span></label>


            <section id="content" class="tab-content">
                <h3 id="content1">O meu carro foi <b>roubado</b></h3>
                <h3 id="content2">Eu vi um carro <b>abandonado</b></h3>
                
                        {{ csrf_field() }}

                            <fieldset class="form-group">
                                <label for="inputEmail3">Matrícula*</label>
                                <div>
                                  <input type="text" name="matricula" class="form-control matricula" id="matricula" value="{{ old('matricula') }}" placeholder="20-GA-30" required>
                                </div>
                                <div class="error"> 
                                  @if ($errors->has('matricula'))
                                    <li>A matrícula é um campo obrigatório.</li>
                                  @endif
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                               <label for="inputEmail3">Descrição</label>
                               <div >
                                 <textarea name="descricao" class="form-control" rows="6" placeholder="Descrição">{{ old('descricao') }}</textarea>
                               </div>
                            </fieldset>

                          <fieldset class="form-group">
                              <div class="titulo_separador"><span>Dados do veículo</span></div>

                              <div class="row">

                                <div class="col-md-6">
                                    <label>Marca*</label>
                                    <div>
                                        <select name="marca" id="marca" class="form-control" required>
                                            <option disabled selected value="">- Marca -</option>
                                            @foreach ($marcas as $marca)
                                                <option value="{{$marca->id}}" @if (old('marca') == $marca->id) selected @endif>{{$marca->marca}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="error"> 
                                      @if ($errors->has('marca'))
                                        <li>A marca é um campo obrigatório.</li>
                                      @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="modelo">Modelo</label>
                                    <div>
                                      <input type="text" name="modelo" class="form-control" value="{{ old('modelo') }}" placeholder="Focus 1.6 TDI">
                                    </div>
                                </div>

                            </div>

                         </fieldset>

                         <fieldset class="form-group">

                            <div class="titulo_separador"><span>Última Localização</span></div>

                            <label>Concelho*</label>
                            <select required name="localizacao" id="concelho" class="form-control">
                                <option selected disabled>Concelho</option>
                                    @foreach ($concelhos as $concelho)
                                        <option value="{{$concelho->id}}" @if (old('localizacao') == $concelho->id) selected @endif>{{$concelho->concelho}}</option>
                                    @endforeach
                            </select>
                            <div class="error"> 
                                @if ($errors->has('concelho'))
                                  <li>O concelho é um campo obrigatório.</li>
                                @endif
                              </div>
                         </fieldset>

                         <fieldset class="form-group">
                            <div class="titulo_separador"><span>Adicionar fotos</span></div>

                            <label>Adicionar fotos</label>

                            <div class="">
                                <div id="image-preview0" class="ip">
                                  <label for="image-upload0" id="image-label0" class="il">Adicionar</label>
                                  <input type="file" name="foto[]" id="image-upload0" class="iu" />
                                </div>

                                <div id="image-preview1" class="ip">
                                  <label for="image-upload1" id="image-label1" class="il">Adicionar</label>
                                  <input type="file" name="foto[]" id="image-upload1" class="iu" />
                                </div>

                                <div id="image-preview2" class="ip">
                                  <label for="image-upload2" id="image-label2" class="il">Adicionar</label>
                                  <input type="file" name="foto[]" id="image-upload2" class="iu" />
                                </div>

                                <div id="image-preview3" class="ip">
                                  <label for="image-upload3" id="image-label3" class="il">Adicionar</label>
                                  <input type="file" name="foto[]" id="image-upload3" class="iu" />
                                </div>

                                <div id="image-preview4" class="ip">
                                  <label for="image-upload4" id="image-label4" class="il">Adicionar</label>
                                  <input type="file" name="foto[]" id="image-upload4" class="iu" />
                                </div>
                            </div>

                            <small><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Não deixe o anúncio sem imagem, caso não tenha nenhuma do seu veículo por favor utilize uma da internet</small>
                         </fieldset>

                         @if (!Auth::check())

                         <fieldset class="form-group">

                            <div class="titulo_separador"><span>Informações Pessoais</span></div>

                            <div class="row">

                                <div class="col-md-6">
                                    <label>Nome*</label>
                                    <input type="text" name="cnome" value="{{ old('nome') }}" class="form-control" placeholder="João Mendes" required>

                                    <div class="error"> 
                                      @if ($errors->has('nome'))
                                        <li>O nome é um campo obrigatório.</li>
                                      @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label>Email*</label>
                                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="alguem@email.com" required>

                                    <div class="error"> 
                                      @if ($errors->has('email'))
                                        <li>O email é um campo obrigatório.</li>
                                      @endif
                                    </div>
                                </div>

                            </div>

                            <small style="margin-top: 10px;"><i class="fa fa-lock yellow" aria-hidden="true"></i> O seu nome e email não serão revelados.</small>

                        </fieldset>

                            

                        <fieldset class="form-group">

                            <div class="titulo_separador"><span>A minha conta</span></div>

                            <div class="row">
                                <div>
                                    <div class="col-md-4">
                                        <label>Utilizador*</label>
                                        <input type="text" name="cutilizador" value="{{ old('utilizador') }}" class="form-control" placeholder="Nome de utilizador" required>

                                        <div class="error"> 
                                          @if ($errors->has('utilizador'))
                                            <li>O utilizador é um campo obrigatório.</li>
                                          @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Password*</label>
                                        <input type="password" name="cpassword" class="form-control" placeholder="********" required>
                                        <div class="error">
                                          @foreach ($errors->get('password') as $message)
                                              <span>{{$message}}</span>
                                          @endforeach
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Confirmação Password*</label>
                                        <input type="password" name="cpassword_confirmation" class="form-control" placeholder="********" required>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        @endif

                        <fieldset class="form-group">
                            <button type="submit" class="btn btn-primary pull-right">Criar Anúncio</button>
                        </fieldset>
              </section>
            </form>
            
        </div>
                    
              </div>
            </div>
          </div>
        </section>
          
        @extends('layouts.footer')
    
        @extends('layouts.login')

    </body>
</html>
