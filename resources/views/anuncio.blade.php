<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        
        @include('layouts.head.tracking')

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/nouislider.min.css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/nouislider.min.js"></script>
        

        <!-- Place this data between the <head> tags of your website -->
        <title>{{$anuncio->marca->marca}} {{$anuncio->modelo}} - {{$anuncio->matricula}} - Carro {{$anuncio->categoria}} | Perdi o Carro</title>
        <meta name="description" content="{{ $anuncio->descricao }}"/>

        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="{{$anuncio->marca->marca}} {{$anuncio->modelo}} - {{$anuncio->matricula}} - Carro {{$anuncio->categoria}}"/>
        <meta itemprop="description" content="{{ $anuncio->descricao }}"/>
        <meta itemprop="image" content="{{ secure_url('storage/'.$anuncio->foto->caminho) }}"/>

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@perdiocarro"/>
        <meta name="twitter:title" content="{{$anuncio->marca->marca}} {{$anuncio->modelo}} - {{$anuncio->matricula}} - Carro {{$anuncio->categoria}}"/>
        <meta name="twitter:description" content="{{ $anuncio->descricao }}"/>
        <meta name="twitter:creator" content="@perdiocarro"/>
        <!-- Twitter summary card with large image must be at least 280x150px -->
        <meta name="twitter:image:src" content="{{ secure_url('storage/'.$anuncio->foto->caminho) }}"/>

        <!-- Open Graph data -->
        <meta property="og:title" content="{{$anuncio->marca->marca}} {{$anuncio->modelo}} - {{$anuncio->matricula}} - Carro {{$anuncio->categoria}}" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="{{Request::url()}}" />
        <meta property="og:image" content="{{ secure_url('storage/'.$anuncio->foto->caminho) }}" />
        <meta property="og:image:width" content="{{ $width }}" />
        <meta property="og:image:height" content="{{ $height }}" />
        <meta property="og:description" content="{{ $anuncio->descricao }}" />
        <meta property="og:site_name" content="Perdi o Carro" />

        <meta property="fb:app_id" content="1255430974572704" />
    </head>
    <body>

        @include('layouts.header')

        @if (App::environment('production'))
            <section>
                <div class="container">
                    <div class="col-md-12 text-center anuncio">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Anuncio responsivo -->
                        <ins class="adsbygoogle"
                            style="display:block"
                            data-ad-client="ca-pub-9036204650678898"
                            data-ad-slot="7262327761"
                            data-ad-format="auto"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </section>
        @endif

        <section id="corpo">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">

                <div class="panel panel-body">
                    <div><h3 style="margin-bottom: 22px;margin-top: 11px;display: inline-block;">{{$anuncio->marca->marca}} {{$anuncio->modelo}} - {{$anuncio->matricula}}</h3> </div>

                        <div class="carousel slide" id="myCarousel">
                            @if ($anuncio->recuperado == 1) <span class="cs-category-label recuperado">Recuperado</span> @else <span class="cs-category-label {{$anuncio->categoria}}">{{$anuncio->categoria}}</span> @endif
                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                @foreach ($anuncio->fotos as $foto)
                                    <div class="{{ ($loop->iteration == 1 ? 'active' : '')}} item" data-slide-number="{{$loop->iteration}}">
                                        <img src="/storage/{{$foto->caminho}}" class="img-responsive">
                                    </div>
                                @endforeach

                            </div><!-- Carousel nav -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>                                     
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <i class="fa fa-angle-right" aria-hidden="true"></i>                                      
                            </a>                                
                        </div>

                        <div class="row hidden-xs" id="slider-thumbs">
                            <!-- Bottom switcher of slider -->
                            @foreach ($anuncio->fotos as $foto)
                                <li class="col-sm-2">
                                    <a class="thumbnail @if ($loop->first) active @endif" id="carousel-selector-{{$loop->iteration}}" data-slide-to="{{$loop->iteration}}"><img src="/storage/{{$foto->caminho}}"></a>
                                </li>
                            @endforeach                
                        </div>


                    <div class="row">
                        <div class="col-md-12">
                            
                            <table class="table table-responsive" style="background: #fafafa;text-align: center;">
                                <tbody>
                                    <tr>
                                        <td><b>Matrícula</b></td>
                                        <td>{{$anuncio->matricula}}</td>
                                        <td><b>Marca</b></td>
                                        <td>{{$anuncio->marca->marca}}</td>
                                    </tr>

                                    <tr>
                                        <td><b>Modelo</b></td>
                                        <td>{{$anuncio->modelo}}</td>
                                        <td><b>Localidade</b></td>
                                        <td>{{$anuncio->concelho->concelho}}, Portugal</td>
                                    </tr>

                                </tbody>
                            </table>

                            <hr>

                            <p class="anuncio-desc">{{ $anuncio->descricao }} </p>

                            <hr>
                            <script src="https://cdn.jsdelivr.net/sharer.js/latest/sharer.min.js"></script>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                   
                                    <div class="social-buttons text-center">
                                        <div class="col-md-4 col-xs-12">
                                            <button class="btn-share facebook sharer" data-sharer="facebook" data-url="https://perdiocarro.pt/anuncio/{{$anuncio->slug}}">
                                               <i class="fa fa-facebook-official"></i> Partilhe no Facebook
                                            </button>
                                        </div>

                                        <div class="col-md-4 col-xs-12">
                                            <button class="btn-share twitter sharer" data-title="Mais um carro roubado ..." data-sharer="twitter" data-url="https://perdiocarro.pt/anuncio/{{$anuncio->slug}}">
                                                <i class="fa fa-twitter-square"></i> Partilhe no Twitter
                                            </button>
                                        </div>

                                        <div class="col-md-4 col-xs-12">
                                            <button class="btn-share google sharer" data-sharer="googleplus" data-url="https://perdiocarro.pt/anuncio/{{$anuncio->slug}}">
                                               <i class="fa fa-google-plus-square"></i> Partilhe no G+
                                            </button>
                                        </div>

                                    </div>    
                                </div>
                            </div>


                            <!-- Modal -->
                            <div class="modal fade" id="reportar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog modal-md" role="document">
                                <div class="modal-content">
                                  <div class="modal-header" style="background: #d9534f;color: white;text-align: center">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Reportar Anúncio</h4>
                                  </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal" action="" method="POST" role="form">
                                            <div class="form-group">
                                                <label class=" col-md-12" for="">Nome</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" id="" placeholder="O seu nome" value="@if (Auth::check()) {{Auth::user()->name}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class=" col-md-12" for="">Email</label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" id="" placeholder="alguém@provedor.com" value="@if (Auth::check()) {{Auth::user()->email}} @endif">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class=" col-md-12" for="">Motivo</label>
                                                <div class="col-md-12">
                                                    <textarea class="form-control" id="" placeholder="Descreva o motivo pelo qual deseja reportar este anúncio" rows="5"></textarea>
                                                </div>
                                            </div>
                                        
                                            <div class="form-group text-right">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                    <button type="submit" class="btn btn-primary">Reportar</button>
                                                    <input type="hidden" name="anuncio_id" value="{{$anuncio->id}}">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                
              </div>

              <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="panel panel-body">
                     @include('layouts.flash')
                     @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($anuncio->categoria == 'Roubado')
                        <p class="text-muted">Tem informações sobre este automóvel? Entre em contato com o proprietário.</p>
                    @else
                        <p class="text-muted">Tem alguma pergunta sobre este automóvel? Entre em contato com a pessoa que publicou.</p>
                    @endif
                    <form class="form-horizontal" action="/nova-mensagem" method="POST" role="form">

                        @if (!Auth::check())
                            <div class="form-group">
                                <label class="col-md-12" for="">Nome</label>
                                <div class="col-md-12">
                                    <input type="text" name="nome" class="form-control" placeholder="Nome" value="@if (Auth::check()) {{Auth::user()->name}} @endif">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="">Email</label>
                                <div class="col-md-12">
                                    <input type="email" name="email" class="form-control" placeholder="Email" value="@if (Auth::check()) {{Auth::user()->name}} @endif">
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-12" for="">Informações</label>
                            <div class="col-md-12">
                                <textarea rows="3" name="informacoes" class="form-control" placeholder="Informações"></textarea>
                            </div>
                        </div>
                        @if (!Auth::check())
                            @if ($errors->has('humano')) 
                                <span class="alert-danger help-block">
                                    <strong>Prove que é humano.</strong>
                                </span>
                            @endif
                            <div class="form-group">
                                <div class="" id="slider-toggle" name="humano"></div>
                            </div>
                        @endif
                        {{ csrf_field() }}
                        <input type="hidden" name="anuncio" id="inputAnuncio" class="form-control" value="{{$anuncio->id}}">
                        <input type="hidden" name="utilizador" value="@if (Auth::check()){{Auth::user()->id}}@endif">
                        <input type="hidden" name="humano" value="NULL" id="humano">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                </div>

                <div class="panel panel-body">
                    <iframe src="https://www.google.pt/maps/embed/v1/place?key=AIzaSyCln-gbCSFbSW0p3xJZ9yyN8_Lrxc2LPcw&q={{$anuncio->concelho->concelho}}+Portugal" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="corpo" class="relacionados">
            <div class="container">
                <h3>Viu algum destes carros?</h3>
                <div class="row">
                    @foreach($veiculos as $veiculo)
                    <article class="col-md-3 col-sm-4">
                        <div class="directory-section">
                            <div class="cs_thumbsection">
                                <figure>
                                    <a href="/anuncio/{{$veiculo->slug}}" style="background-image: url('/storage/{{$veiculo->foto->caminho}}');">
                                        <span class="cs-category-label {{$veiculo->categoria}}">{{$veiculo->categoria}}</span>                                   
                                    </a>
                                </figure>
                            </div>
                            <div class="cs_details">
                                <ul class="dr_userinfo list-unstyled">
                                    <li><span class="cs-label">Marca</span><span>{{ $veiculo->marca->marca }}</span></li>
                                        <li><span class="cs-label">Modelo</span></small><span>{{$veiculo->modelo}}</span></li>
                                        <li><span class="cs-label">Matrícula</span></small><span>{{$veiculo->matricula}}</span></li>
                                        <li><span class="cs-label">Localidade</span></small><span>{{ $veiculo->concelho->concelho }}</span></li>
                                </ul>
                            </div>
                        </div>
                        
                    </article>
                    @endforeach

                </div>
            </div>
        </section>
          
        @extends('layouts.footer')
    
        @extends('layouts.login')

        <script type="text/javascript">
            jQuery(document).ready(function($) {
 
                $('#myCarousel').carousel({
                        interval: 5000
                });

                $('#myCarousel').on('slid.bs.carousel', function(){
                   var index = $('.carousel-inner .item.active').index() + 1;
                   $('li .thumbnail').removeClass('active');
                   $('li .thumbnail[data-slide-to="'+index+'"]').addClass('active');
                });
         
                $('#carousel-text').html($('#slide-content-0').html());
         
                //Handles the carousel thumbnails
               $('[id^=carousel-selector-]').click( function(){
                    var id = this.id.substr(this.id.lastIndexOf("-") + 1);
                    id = parseInt(id);
                    $('#myCarousel').carousel(id-1);
                    $('li .thumbnail').removeClass('active');
                    $('li .thumbnail[data-slide-to="'+id+'"]').addClass('active');
                });
            

                var toggleSlider = document.getElementById('slider-toggle');
                var toggleSliderValue = document.getElementById('humano');

                noUiSlider.create(toggleSlider, {
                    orientation: "horizontal",
                    start: 0,
                    connect: "lower",
                    range: {
                        'min': [0, 1],
                        'max': 1
                    },

                })

                toggleSlider.noUiSlider.on('update', function( values, handle ){
                    if ( values[handle] === '1' ) {
                        toggleSlider.classList.add('off');
                    } else {
                        toggleSlider.classList.remove('off');
                    }
                });

                toggleSlider.noUiSlider.on('update', function( values, handle ){
                    toggleSliderValue.value = parseInt(values[handle]);
                });
            });
        </script>

    </body>
</html>
