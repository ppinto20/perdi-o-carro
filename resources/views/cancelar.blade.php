<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>

        @include('layouts.head.tracking')

        @include('layouts.head.metas', [
            'titlo' => 'Estamos tristes de o ver partir - Perdi o Carro', 
            'descricao' => 'Partilha de informações sobre carros roubados ou abandonados. Partilhe informações e ajude a comunidade, hoje é para alguém mas amanhã podes ser tu a precisar.']
        )

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">

        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
    </head>
    <body>

        @include('layouts.header')

        <section style="margin-top: 40px; margin-bottom: 40px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-md-offset-">
                        <div class="profile-content" style="max-width: 80%;margin:auto;">
                            <h3><legend>Cancelar email - Perdi o Carro</legend></h3>
                            @include('layouts.flash')
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <p>Estamos tristes de o ver partir, com esta plataforma tentamos ajudar a comunidade a recuperar os carros roubados. Esperemos que reconsidere e nos ajude a ajudar os outros.</p>
                            <hr>
                            
                            <form class="form-horizontal" action="/cancelar-emails" method="POST" role="form">
                                {{ csrf_field() }}
                            
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group col-md-12">
                                            <label for="">Email</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></div>
                                                <input type="email" name="email" class="form-control" placeholder="O seu email" value="@if (Auth::check()) {{Auth::user()->email}} @endif" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Qual é o motivo?</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-comment" aria-hidden="true"></i></div>
                                         <textarea name="mensagem" name="mensagem" class="form-control" rows="3" required="required" placeholder="Escreva a sua mensagem"></textarea>
                                    </div>
                                </div>
                            
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        
        @extends('layouts.footer')
        
        @extends('layouts.login')

    </body>
</html>