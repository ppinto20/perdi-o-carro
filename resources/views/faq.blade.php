<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        
        @include('layouts.head.tracking')

        @include('layouts.head.metas', [
            'titlo' => 'Encontre as respostas às perguntas mais frequentes sobre o Perdi o Carro', 
            'descricao' => 'Encontre as respostas às perguntas mais frequentes, desde dicas de como encontrar o seu carro a como usar a nossa plataforma']
        )

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/nouislider.min.css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/nouislider.min.js"></script>

    </head>
    <body>

        @include('layouts.header')

        <section id="corpo" style="margin-bottom: 35px;">
            <div class="container">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 profile-content">
                    <div id="integration-list">
                    <h3>Perguntas Frequentes</h3>
                    <ul>
                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>Adicionar carro roubado</h2>
                                    <span>O meu carro foi roubado, o que devo fazer?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Infelizmente é uma situação cada vez mais comum mas calma, estamos aqui para o ajudar. Tem duas opções, a primeira é registar-se sem criar um anúncio <a target="_blank" href="/registar">aqui</a> ou então pode registar-se ao mesmo tempo que anuncia o roubo do seu carro <a target="_blank" href="/novo-anuncio">aqui</a> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>Remover o meu carro</h2>
                                    <span>O meu carro foi roubado, o que devo fazer?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Para remover o seu carro basta aceder à sua conta, através do menu "Os Meus Anúncios", aqui pode gerir os seus anúncios e apagar os mesmos se necessário. Se não tem conta e pretende que os dados do seu carro sejam removidos no website por favor contacte <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a>.
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>O meu carro já foi adicionado</h2>
                                    <span>O meu carro já foi adicionado por outra pessoa, o que devo fazer?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Neste caso aconselhamos a criação de uma conta <a href="https://perdiocarro.pt/registar">aqui</a> e que nos envie um email para a <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a> ou utilize o nosso formulário <a href="/contactos">aqui</a> e nós trataremos de fazer as verificações necessárias e atribuir o anúncio ao seu dono.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        

                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>Carro abandonado</h2>
                                    <span>Tenho informações sobre um possível carro abandonado, o que devo fazer?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Se suspeitar que um carro foi/está abandonado não hesite em criar um anúncio selecionando a opção "abandonado" <a target="_blank" href="/novo-anuncio">aqui</a>. Os seus dados não serão partilhados com ninguém e caso alguém precise de o contatar para saber mais detalhes sobre o carro, utilize a nossa plataforma (mensagens) para garantir a sua privacidade.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>Privacidade</h2>
                                    <span>No caso de me registar, como é que lidam com os meus detalhes/dados pessoais?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Esta é uma das principais razões para a criação desta plataforma, nós garantimos que os seus dados pessoais, tais como, nome, email, etc são totalmente confidenciais dentro da nossa plataforma. Não nos responsabilizamos pela partilha de dados pessoais caso utilize alguma ferramenta externa.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>Partilha de informações</h2>
                                    <span>No caso não me queira registar, posso partilhar informações com os proprietários?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Sim pode mas neste caso, o seu nome e email serão apresentados ao proprietário do anúncio. Caso não queira tornar o seu email público terá de se registar primeiro.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a class="expand">
                                <div class="right-arrow">+</div>
                                <div>
                                    <h2>Cancelar emails do Perdi o Carro</h2>
                                    <span>Recebi um email do Perdi o Carro e não quer receber mais, o que devo fazer?</span>
                                </div>
                            </a>

                            <div class="detail">
                                <div id="left" style="width:15%;float:left;height:100%;">
                                    <div id="sup">
                                        <img src="/img/pergunta.png" width="100%" />
                                    </div>
                                </div>
                                <div id="right" style="width:85%;float:right;height:100%;padding-left:20px;">
                                    <div id="sup">
                                        <div><span>Ficamos triste de o ver partir, nós nunca enviamos SPAM e apenas estamos a tentar ajudar a sociedade. Caso não deseje receber mais notificações do Perdi O carro basta preencher o formulário <a href="https://perdiocarro.pt/cancelar-emails">aqui</a> ou enviar um email para a <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a> ou utilize o nosso formulário <a href="/contactos">aqui</a> e nós removeremos o seu email.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <hr>
                    <h3 class="text-center">Ainda com dúvidas? <br><br>Envie um email para <a href="mailto:info@perdiocarro.pt">info@perdiocarro.pt</a> ou utilize o nosso formulário <a href="/contactos">aqui</a>.</h3>
                </div>
            </div>
        </section>
     @extends('layouts.footer')
    
     @extends('layouts.login')

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".expand").on( "click", function() {
                    $(this).next().slideToggle(200);
                    $expand = $(this).find(">:first-child");
                    
                    if($expand.text() == "+") {
                      $expand.text("-");
                    } else {
                      $expand.text("+");
                    }
                });
            });
        </script>

    </body>
</html>