<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        
        @include('layouts.head.tracking')

        @include('layouts.head.metas', [
            'titlo' => 'Partilhe informações sobre carros roubados ou abandonados', 
            'descricao' => 'Partilha de informações sobre carros roubados ou abandonados. Partilhe informações e ajude a comunidade, hoje é para alguém mas amanhã podes ser tu a precisar.']
        )


        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
        <script type="text/javascript" src="/js/jquery.mask.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/2.1.0/jquery.infinitescroll.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#matricula').mask('AA-AA-AA');
                $("#marca").select2();

                var loading_options = {
                    finishedMsg: "<div class='end-msg'>Não há mais anúncio disponíveis de momento.</div>",
                    msgText: "<div class='center'>A carregar mais anúncios ...</div>",
                    img: "/img/loading.gif",
                    speed: "slow"
                };

                $('#veiculos').infinitescroll({
                  loading : loading_options,
                  navSelector : "#veiculos .pagination",
                  nextSelector : "#veiculos .pagination li.active + li a",
                  itemSelector : "article"
                });

                $('#veiculos').infinitescroll('pause');
            });

        </script>

        
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v2.11&appId=1255430974572704';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        @include('layouts.header')

        <section id="pesquisar">
            @include('layouts.pesquisa')
        </section>

        @if (App::environment('production'))
            <section>
                <div class="container">
                    <div class="col-md-12 text-center anuncio">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- Topo -->
                        <ins class="adsbygoogle"
                            style="display:block"
                            data-ad-client="ca-pub-9036204650678898"
                            data-ad-slot="4308861365"
                            data-ad-format="auto"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </section>
        @endif

        <section id="corpo">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <h3>Carros adicionados</h3>
                
                @if ($veiculos->isEmpty())
                    <p>Não foram encontrados resultados</p>
                @else
                    <div id="veiculos">
                        @foreach ($veiculos as $veiculo)
                            <article class="col-md-4 col-sm-6">
                                <div class="directory-section">
                                    <div class="cs_thumbsection">
                                        <figure class="{{$veiculo->categoria}}">
                                            <a href="/anuncio/{{$veiculo->slug}}" style="background-image: url('/storage/{{$veiculo->foto->caminho}}');">
                                                @if ($veiculo->recuperado == 1) <span class="cs-category-label recuperado">Recuperado</span> @else <span class="cs-category-label">{{$veiculo->categoria}}</span> @endif                                  
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="cs_details">
                                        <ul class="dr_userinfo list-unstyled">
                                            <li><span class="cs-label">Marca</span><span>{{ $veiculo->marca->marca }}</span></li>
                                            <li><span class="cs-label">Modelo</span></small><span>{{$veiculo->modelo}}</span></li>
                                            <li><span class="cs-label">Matrícula</span></small><span>{{$veiculo->matricula}}</span></li>
                                            <li><span class="cs-label">Localidade</span></small><span>{{ $veiculo->concelho->concelho }}</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </article>

                        @endforeach

                        <div class="hidden">
                            {{ $veiculos->links() }}
                        </div> 

                        <div class="col-md-12" id="carregar-mais">
                            <button class="btn btn-primary center-block" onclick="$('#veiculos').infinitescroll('retrieve');$('#veiculos').infinitescroll('toggle');$(this).fadeOut('slow');">Carregar mais anúncios</button>
                        </div>

                    </div>
                @endif
              </div>


              @include('layouts.sidebarfiltro')
              

            </div>
          </div>
        </section>
          
        @extends('layouts.footer')
    
        @extends('layouts.login')

    </body>
</html>
