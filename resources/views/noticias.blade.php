<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        
        @include('layouts.head.tracking')

        @include('layouts.head.metas', [
            'titlo' => 'Últimas noticias relacionadas com o roubo de automóveis', 
            'descricao' => 'Partilha de informações sobre carros roubados ou abandonados. Partilhe informações e ajude a comunidade, hoje é para alguém mas amanhã podes ser tu a precisar.']
        )

        <!-- STYLESHEET -->
        <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="/css/nouislider.min.css">
        <link href="/css/style.css" rel="stylesheet" type="text/css">


        <!-- SCRIPTS -->
        <script type="text/javascript" src="/js/app.js"></script>
        <script type="text/javascript" src="/js/nouislider.min.js"></script>

    </head>
    <body>

    @include('layouts.header')
    <section class="publicaciones-blog-home">
      <div class="container">
        <div class="">
          <h2>Últimas  <b>Notícias</b></h2>
          <div class="row-page row">
            <div class="col-page col-sm-8 col-md-6">
              <a target="_blank" href="https://www.rtp.pt/noticias/pais/psp-desmantela-rede-de-roubo-e-desmontagem-de-automoveis_v988991" class="black fondo-publicacion-home">
                <div class="img-publicacion-principal-home">
                  <img class="" src="/storage/noticias/PSP-desmantela.png">
                </div>
                <div class="contenido-publicacion-principal-home">
                  <h3>PSP desmantela rede de roubo e desmontagem de automóveis</h3>
                  <p>A PSP diz que se trata de uma das maiores operações policiais de sempre. Desmantelou uma rede que se dedicava ao roubo de carros<span>...</span></p>
                </div>
                <div class="mascara-enlace-blog-home">
                  <span>Fonte: RTP </span>
                </div>
              </a>
            </div>
            <div class="col-page col-sm-4 col-md-3">
              <a target="_blank" href="http://www.cmjornal.pt/portugal/detalhe/detidos-por-mais-de-300-furtos"  class="fondo-publicacion-home">
                <div class="img-publicacion-home">
                  <img class="img-responsive" src="/storage/noticias/cm-not1.jpg">
                </div>
                <div class="contenido-publicacion-home">
                  <h3>Detidos por mais de 300 furtos Sob violência e ameaças, roubavam idosos.</h3>
                  <p>Marco ‘Pia’, que conduzia um carro roubado e que fugiu após ter abalroado um veículo da PSP - que terminou na morte de ‘Pika’,<span>...</span></p>
                </div>
                <div class="mascara-enlace-blog-home">
                  <span>Fonte: CM </span>
                </div>
              </a>
            </div>
            <div class="col-page col-sm-4 col-md-3">
              <a target="_blank" href="http://www.jn.pt/justica/interior/desmantelada-em-espanha-uma-rede-de-trafico-de-carros-roubados-em-portugal-5331399.html" class="fondo-publicacion-home">
                <div class="img-publicacion-home">
                  <img class="img-responsive" src="/storage/noticias/jn-not1.jpg">
                </div>
                <div class="contenido-publicacion-home">
                  <h3>Desmantelada em Espanha rede de tráfico de carros roubados em Portugal</h3>
                  <p>A Guarda Civil de Espanha desmantelou uma rede de tráfico de carros roubados em Portugal e <span>...</span></p>
                </div>
                <div class="mascara-enlace-blog-home">
                  <span>Fonte: JN </span>
                </div>
              </a>
            </div>
<!--             <div class="col-page col-sm-4 col-md-3">
              <a href="" class="fondo-publicacion-home">
                <div class="img-publicacion-home">
                  <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                </div>
                <div class="contenido-publicacion-home">
                  <h3>Neque porro quisquam est qui dolorem ipsum</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed placerat porta ex, sed ullamcorper ipsum lacinia nec.<span>...</span></p>
                </div>
                <div class="mascara-enlace-blog-home">
                  <span>Lorem </span>
                </div>
              </a>
            </div>
            <div class="hidden-sm col-page col-sm-4 col-md-3">
              <a href="" class="fondo-publicacion-home">
                <div class="img-publicacion-home">
                  <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                </div>
                <div class="contenido-publicacion-home">
                  <h3>Neque porro quisquam est qui dolorem ipsum</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed placerat porta ex, sed ullamcorper ipsum lacinia nec.<span>...</span></p>
                </div>
                <div class="mascara-enlace-blog-home">
                  <span>Lorem </span>
                </div>
              </a>
            </div>
            <div class="hidden-sm col-page col-sm-4 col-md-3">
              <a href="" class="fondo-publicacion-home">
                <div class="img-publicacion-home">
                  <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                </div>
                <div class="contenido-publicacion-home">
                  <h3>Neque porro quisquam est qui dolorem ipsum</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed placerat porta ex, sed ullamcorper ipsum lacinia nec.<span>...</span></p>
                </div>
                <div class="mascara-enlace-blog-home">
                  <span>Lorem </span>
                </div>
              </a>
            </div>
            <div class="col-page col-sm-4 col-md-3">
              <a href="#" class="todas-las-publicaciones-home">
                  <span>Neque porro quisquam est qui dolorem ipsum</span>
              </a>
            </div> -->
          </div>
        </div>
      </div>

      @if (App::environment('production'))
        <div class="container">
          <div class="col-md-12 text-center anuncio">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Noticias responsivo -->
            <ins class="adsbygoogle"
                style="display:block"
                data-ad-client="ca-pub-9036204650678898"
                data-ad-slot="2692527365"
                data-ad-format="auto"></ins>
            <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
          </div>
        </div>
      @endif
    </section>

    @extends('layouts.footer')
        
    @extends('layouts.login')

    </body>
</html>
